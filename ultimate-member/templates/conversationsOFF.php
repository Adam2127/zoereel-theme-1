<?php

$i = 0; if ( $conversations ) {
// echo '<pre>';
// echo um_profile_id();
// print_r ($conversations);
// echo '</pre>';
?>

<div class="um um-viewing">
	<div class="um-message-conv">

		<?php		
			$um_profile_id = ($user_id == 0) ? $artistpost_id : um_profile_id();
			$isartist = $user_id;
			$user_id = ($user_id == 0) ? get_the_author_meta('ID') : $user_id;
		
		foreach( $conversations as $conversation ) {

			// if ( $conversation->user_a == um_profile_id() ) {
			// if ( $conversation->user_a == $um_profile_id ) {	
			// 	$user = $conversation->user_b;
			// } else {
			// 	$user = $conversation->user_a;
			// }
			if ( $conversation->user_a == $um_profile_id ) {	
				$user = $conversation->user_b;
			} else {
				$user = $conversation->user_a;
			}
			
			if ( $um_messaging->api->blocked_user( $user ) ) continue;
			if ( $um_messaging->api->hidden_conversation( $conversation->conversation_id ) ) continue;

			$i++;

			if ( $i == 1 && !isset( $current_conversation ) ) {
				$current_conversation = $conversation->conversation_id;
			}

			um_fetch_user( $user );

			$user_name = ( um_user('display_name') ) ? um_user('display_name') : __('Deleted User','um-messaging');

			$is_unread = $um_messaging->api->unread_conversation( $conversation->conversation_id, um_profile_id() );

		?>

		<span href="<?php echo add_query_arg('conversation_id', $conversation->conversation_id ); ?>" class="um-message-conv-item <?php if ( $conversation->conversation_id == $current_conversation ) echo 'active '; ?>" data-message_to="<?php echo $user; ?>" data-trigger_modal="conversation" data-conversation_id="<?php echo $conversation->conversation_id; ?>" data-object_id="<?php echo $conversation->object_id; ?>">

			<span class="um-message-conv-name"><?php echo $user_name; ?></span>

			<span class="um-message-conv-pic"><?php echo get_avatar( $user, 40 ); ?></span>

			<?php if ( $is_unread ) { ?><span class="um-message-conv-new"><i class="um-faicon-circle"></i></span><?php } ?>

			<?php do_action('um_messaging_conversation_list_name'); ?>

		</span>

		<?php } ?>

	</div>

	<div class="um-message-conv-view">

		<?php
		$i = 0;
		foreach( $conversations as $conversation ) {

				if ( isset( $current_conversation ) && $current_conversation != $conversation->conversation_id )
					continue;

				if ( ($conversation->user_a == $um_profile_id) ) {
				 $user = $conversation->user_b;
				} else {
				 // $user = $conversation->user_a;
				 $user = get_the_author_meta('ID');
				}
				if ( $um_messaging->api->blocked_user( $user ) ) continue;
				if ( $um_messaging->api->hidden_conversation( $conversation->conversation_id ) ) continue;

				$i++; if ( $i > 1 ) continue;

				um_fetch_user( $user );

				$user_name = ( um_user('display_name') ) ? um_user('display_name') : __('Deleted User','um-messaging');
echo 'user is : ' . $user . ' and user_id is: ' . $user_id;
				// $um_messaging->api->conversation_template( $user, $user_id );
				// $user_id = ($isartist == 0) ? $artistpost_id : $user_id;
				conversation_zoereel_template($user, $user_id, $artistpost_id, $isartist);

		}
	?>

	</div><div class="um-clear"></div>
</div>

<?php } else { ?>

<div class="um-message-noconv">
	<i class="um-icon-android-chat"></i>
	<?php _e('No chats found here','um-notifications'); ?>
</div>

<?php } ?>
