<?php
	global $ultimatemember;

	$commenttype = 'comments'; $commenttypeText = 'Comments'; $reeltalk = false;
	// $commenttype = 'reeltalk'; $commenttypeText = 'Reeltalk'; $reeltalk = true;
	$artistVidIds = array(); $profileID = um_profile_id(); $video_ids = array(); $myProfile = ($profileID == get_current_user_id()) ? true : false;

		$query = new WP_Query( array( 'author' => $profileID ) );
		if ( $query->have_posts() ) :
			if ( $myProfile ) {	?>
				<ul class="nav nav-pills">
					<li><a class="nav-link active" data-toggle="pill" href="#commentsOf"><?php $myProfile ? _e('My Videos','zoereel') : _e('Artist Videos','zoereel');?></a></li>
					<li><a class="nav-link" data-toggle="pill" href="#commentsBy"><?php _e('Other Videos','zoereel');?></a></li>
				</ul>
			<?php } ?>
				<div class="tab-content">
				    <section class="tab-pane active" id="commentsOf">
<?php
		endif;
		wp_reset_postdata();

		$args = array(
			'post_author__in' => $profileID,
			'type'=> $commenttype
		);
		$currentVidId = 0; $counter = 0; $commentmade = false; $commentVidIdViewAll;
		$vidcomments = get_comments($args);

		foreach($vidcomments as $vidcomment) :

				$vidcommentID = $vidcomment->comment_ID; $vidID = $vidcomment->comment_post_ID; $commentsNum = get_comments_number($vidID); array_push($artistVidIds, $vidID);

$commentsNum = get_comments(
	array(
		'status' => 'approve',
		'post_id'=> $vidID,
		'type'=> $commenttype,
		'count' => true)
);
if ($commentsNum < 1) { return; }

	 			if ( $vidID == $currentVidId && $counter > 2 ) {
	 				// continue

	 			} elseif ($vidID == $currentVidId ) {
	 				$counter++;
	 				include( locate_template( 'partials/comment-text.php', false, false ) );
	 			} elseif ( $currentVidId == 0 ) {
					$commentmade = true; $counter++; $currentVidId = $vidID; $commentVidIdViewAll = $vidcommentID;
?>
	 							<article class="um-item">
	 								<?php include( locate_template( 'partials/comment-poster.php', false, false ) ); ?>
	 								<div class="zr-item-desc">
	 									<?php include( locate_template( 'partials/comment-meta.php', false, false ) ); ?>
<?php		    } else {
					$counter = 0; $currentVidId = $vidID;
?>
	 								</div> <!-- end zr-item-desc -->
	 								<div class="center"><a href="<?php echo get_comment_link( $commentVidIdViewAll ); ?>" class="btn btn-secondary btn-sm"><b><?php echo 'View All ' . $commenttypeText; ?></b></a></div>
	 							</article> <!-- end um-item -->
	 							<?php $commentVidIdViewAll = $vidcommentID; ?>
	 							<article class="um-item">
	 								<?php include( locate_template( 'partials/comment-poster.php', false, false ) ); ?>
	 								<div class="zr-item-desc <?php echo $vidcomment->comment_post_ID; ?>">
	 									<?php include( locate_template( 'partials/comment-meta.php', false, false ) ); ?>
<?php		    }

		endforeach;

			if ($commentmade) {
?>
									</div> <!-- end zr-item-desc last one-->
									<div class="center"><a href="<?php echo get_comment_link( $commentVidIdViewAll ); ?>" class="btn btn-secondary btn-sm"><b><?php echo 'View All ' . $commenttypeText; ?></b></a></div>
								</article> <!-- end um-item -->
							</section> <!-- end of id="commentsOf" -->
<?php		} // ################## End MY VIDEOS comments ################# // ?>

<?php if ( $myProfile ) { // echo '</section> <!-- end of id="commentsOf" -->';

// ISSUE: Don't know why "active" class doesn't toggle for pill <li> link https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_pills_dynamic added js below in meantime
?>
<script type="text/javascript">
	jQuery( window ).load(function() {
		jQuery('.nav-link').click(function(){
			if (!jQuery(this).hasClass('active') ) {
				jQuery('a.active').removeClass('active');
			}
		});
	});
</script>
					<section class="tab-pane fadeOFF" id="commentsBy">
						<!--<span class="descriptor"><b>The latest on videos you have commented on</b></span> -->
							<?php
								$artistVidIds = array_unique($artistVidIds);
									$args = array(
										'user_id' => $profileID,
										'type'=> $commenttype,
										'post__not_in' => '1824',
									);
									$vidcomments = get_comments($args);

							$videoAllID = wp_list_pluck($vidcomments, 'comment_post_ID');
							$videoIDs = array_unique($videoAllID);

							foreach($videoIDs as $vidID) :

							$commentsNum = get_comments(
								array(
									'status' => 'approve',
									'post_id'=> $vidID,
									'type'=> $commenttype,
									'count' => true)
							);
							if ($commentsNum < 1) { return; }
							?>
									<article class="um-item">
								 		<?php include( locate_template( 'partials/comment-poster.php', false, false ) ); ?>
								 		<div class="zr-item-desc <?php echo $vidcomment->comment_post_ID; ?>">
							<?php
							$args = array(
									'post_id' => $vidID,
									'type' => $commenttype,
									'number' => 3,
								);
							$vidcomments = get_comments($args); $i=0;

								foreach($vidcomments as $vidcomment) :
									$i++; $vidcommentID = $vidcomment->comment_ID; $vidID = $vidcomment->comment_post_ID; $commentsNum = get_comments_number($vidID);
									if($i==1) {
										include( locate_template( 'partials/comment-meta.php', false, false ) );
									} else {
										include( locate_template( 'partials/comment-text.php', false, false ) );
									}
								endforeach;
							?>
								 		</div><!-- end zr-item-desc last one-->
										<div class="center"><a href="<?php echo get_comment_link( $vidcommentID ); ?>" class="btn btn-secondary btn-sm"><b><?php echo 'View All ' . $commenttypeText; ?></b></a></div>
									</article>

							<?php
							endforeach;
} // ################## End if ( $myProfile ) ################# //

	echo '</div><!-- end of tab-content -->';
?>


	<?php if ( isset($ultimatemember->shortcodes->modified_args) && count($ultimatemember->shortcodes->loop) >= 10 && ( 1 == 0 ) ) { // NOTE: added 1 == 0 to disable load more ajax ?>
		<div class="um-load-items">
			<a href="#" class="um-ajax-paginate um-button" data-hook="um_load_comments" data-args="<?php echo $ultimatemember->shortcodes->modified_args; ?>"><?php _e('load more comments','ultimate-member'); ?></a>
		</div>
	<?php } ?>