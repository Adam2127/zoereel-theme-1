<?php
	$profileID = um_profile_id(); $video_ids = array();

	if ( $profileID == get_current_user_id() ) {

		$query = new WP_Query( array( 'author' => $profileID ) );
		if ( $query->have_posts() ) : 			
			?>
				<ul class="nav nav-pills">	
					<li><a class="nav-link active" data-toggle="pill" href="#commentsOf"><?php _e('My Videos','zoereel');?></a></li>
					<li><a class="nav-link" data-toggle="pill" href="#commentsBy"><?php _e('Other Videos','zoereel');?></a></li>
				</ul>
				<div class="tab-content">
				    <section class="tab-pane active" id="commentsOf">
				      	
<?php				
			while ( $query->have_posts() ) : $query->the_post();			
				$video_ids[] = get_the_ID();
			endwhile; 
		endif;
		wp_reset_postdata();	
		
		$args = array(
			'post__in' => $video_ids
		);
		$currentVidId = 0; $counter = 0; $commentmade = false;
		$vidcomments = get_comments($args);
$output = array();
echo '<pre>';
print_r($vidcomments);		
echo '</pre>';
		foreach($vidcomments as $vidcomment) :
			
			if ($vidcomment->comment_type != 'reeltalk') {
				
				$vidcommentID = $vidcomment->comment_ID; $vidID = $vidcomment->comment_post_ID; $commentsNum = get_comments_number($vidID);
			
			
	 			if ( $vidID == $currentVidId && $counter > 2 ) { 
	 				// continue 		
	 			} elseif ($vidID == $currentVidId ) { 
	 				$counter++; // $currentVidId = $vidID;	 		
$output[$vidID] .=	                   '<div class="um-item-link"><a href="'. get_comment_link( $vidcommentID ) . '"><i class="um-icon-chatboxes"></i></a>'.get_comment_excerpt( $vidcommentID ).'</div>';	 	
			    } elseif ( $currentVidId == 0 ) { 
					$commentmade = true;
					$counter++; $currentVidId = $vidID;		 		
$output[$vidID] .=	 			'<article class="um-item">
									<div class="zr-item-poster">
									   <a href="' . get_the_permalink($vidID) .'">'.get_the_post_thumbnail( $vidID, 'medium' ).'</a>
									   <div class="content-overlay">
									   	<a class="play-icon-wrap" href="'.get_the_permalink($vidID).'">
									    	 	<div class="play-icon-wrap-rel-ring"></div>
									   	 	<span class="play-icon-wrap-rel-play"><i class="fa fa-play fa-2x" aria-hidden="true"></i></span>
									    	</a>
									   </div>
									</div>									
									<div class="zr-item-meta">
	 									<span>On <a href="'.get_permalink($vidID).'">'.get_the_title($vidID).'</a></span> <span>' . ($commentsNum == 1) ? '1 comment' :  $commentsNum . ' comments' . '</span>				
	 									<div class="um-item-link"><a href="'. get_comment_link( $vidcommentID ).'"><i class="um-icon-chatboxes"></i></a>'.get_comment_excerpt( $vidcommentID ).'</div>';	
	 			} else { 
					$counter = 0; $currentVidId = $vidID;			
$output[$vidID] .=	 				'</div> <!-- end zr-item-meta -->						
	 							</article> <!-- end um-item -->		
	 			
	 							<article class="um-item">
	 								<div class="zr-item-poster">
	 								   <a href="' . get_the_permalink($vidID) . '">' . get_the_post_thumbnail( $vidID, 'medium' ) . '</a>
	 								   <div class="content-overlay">
	 								   	<a class="play-icon-wrap" href="'.get_the_permalink($vidID).'">
	 								    	 	<div class="play-icon-wrap-rel-ring"></div>
	 								   	 	<span class="play-icon-wrap-rel-play"><i class="fa fa-play fa-2x" aria-hidden="true"></i></span>
	 								    	</a>
	 								   </div>
	 								</div>
	 								<div class="zr-item-meta">
	 									<span>On <a href="'.get_permalink($vidID).'">'.get_the_title($vidID).'</a></span> <span>' . ($commentsNum == 1) ? '1 comment' :  $commentsNum . ' comments' . '</span>				
	 									<div class="um-item-link"><a href="' . get_comment_link( $vidcommentID ) . '"><i class="um-icon-chatboxes"></i></a>' . get_comment_excerpt( $vidcommentID ) . '</div>';
	 			} 		
			}	
		endforeach;
			if ($commentmade) {
// foreach ($output as $item) {
//     echo $item;
// }			
		?>
									<div class="center"><a href="<?php echo get_comment_link( $vidcommentID ); ?>" class="btn btn-secondary btn-sm"><b><?php _e('View All Comments','zoereel');?></b></a></div>
								</div> <!-- end zr-item-meta -->						
						</article> <!-- end um-item -->			
					</section> <!-- end of id="commentsOf" -->
<?php		}
// ISSUE: Don't know why "active" class doesn't toggle for pill <li> link https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_pills_dynamic added js below in meantime
?>
<script type="text/javascript">
	jQuery( window ).load(function() {
		jQuery('.nav-link').click(function(){
			if (!jQuery(this).hasClass('active') ) {
				jQuery('a.active').removeClass('active');
			}
		});
	});
</script>	
<?php
	}	
?>	
					<section class="tab-pane fade" id="commentsBy">
<?php 
		$currentVidId = 0; $counter = 0; $commentmade = false;
		
		foreach( $ultimatemember->shortcodes->loop as $vidcomment ) { 	
			if ($vidcomment->comment_type != 'reeltalk') {

				$vidcommentID = $vidcomment->comment_ID; $vidID = $vidcomment->comment_post_ID; $commentsNum = get_comments_number($vidID);
				
	 			// if ( $vidID == $currentVidId && $counter > 2 ) { 
	 			if ( ($vidID == $currentVidId && $counter >= 2 ) || in_array($vidID, $video_ids) ) {
	 				// continue
	 		  	} elseif ($vidID == $currentVidId ) { 
	 			  	$counter++; $currentVidId = $vidID;
?>	 	
	 				<div class="um-item-link"><a href="<?php echo get_comment_link( $vidcommentID ); ?>"><i class="um-icon-chatboxes"></i></a><?php echo get_comment_excerpt( $vidcommentID ); ?></div>		
	 	
<?php	 		} elseif ( $currentVidId == 0 ) { 
					$counter++; $currentVidId = $vidID;	
?>	 	
	 						<article class="um-item">
	 							<?php include( locate_template( 'partials/comment-poster.php', false, false ) ); ?>	
	 							<div class="zr-item-meta">
	 								<?php include( locate_template( 'partials/comment-meta.php', false, false ) ); ?>	 		
<?php	 		} else { 
					$counter = 0; $currentVidId = $vidID;	
?>	 	
	 								<div class="center"><a href="<?php echo get_comment_link( $vidcommentID ); ?>" class="btn btn-secondary btn-sm"><b><?php _e('View All Comments','zoereel');?></b></a></div>
	 							</div> <!-- end zr-item-meta -->						
	 						</article> <!-- end um-item -->
	 						<article class="um-item">																												 		
	 							<?php include( locate_template( 'partials/comment-poster.php', false, false ) ); ?>
	 							<div class="zr-item-meta <?php echo $vidcomment->comment_post_ID; ?>">																	 		
	 								<?php include( locate_template( 'partials/comment-meta.php', false, false ) ); ?>
<?php	 		} 
			// $currentVidId = $vidID;
			}
		}
			if ($commentmade) {
		?>
									<div class="center"><a href="<?php echo get_comment_link( $vidcommentID ); ?>" class="btn btn-secondary btn-sm"><b><?php _e('View All Comments','zoereel');?></b></a></div>
								</div> <!-- end zr-item-meta -->						
						</article> <!-- end um-item -->			
					</section> <!-- end of id="commentsBy" -->
<?php		}		
	echo '</div><!-- end of tab-content -->';
?>







	
	<?php if ( isset($ultimatemember->shortcodes->modified_args) && count($ultimatemember->shortcodes->loop) >= 10 && ( 1 == 0 ) ) { // NOTE: added 1 == 0 to disable load more ajax ?>	
		<div class="um-load-items">
			<a href="#" class="um-ajax-paginate um-button" data-hook="um_load_comments" data-args="<?php echo $ultimatemember->shortcodes->modified_args; ?>"><?php _e('load more comments','ultimate-member'); ?></a>
		</div>		
	<?php } ?>