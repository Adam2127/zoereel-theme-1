Hi {display_name},

Thank you for signing up with {site_name}! Your account is now active.

To login please visit the following url:

{login_url}

Your account e-mail: {email}
Your account username: {username}

Please Log in and edit your profile to complete your registration.

If you have any problems, please contact us at info@zoereel.com

Thanks,
{site_name}