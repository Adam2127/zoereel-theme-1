Hi {display_name},

Thank you for signing up with {site_name}! To activate your account, please click the link below to confirm your email address:

{account_activation_link}

If you have any problems, please contact us at info@zoereel.com

Thanks,
{site_name}