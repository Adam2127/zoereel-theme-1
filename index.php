<?php // get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<section class="row">
	<div class="row__inner">
		<?php // Loop
		while (have_posts()) : the_post();
			get_template_part('templates'.$isMobile.'/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
		endwhile;
		wp_reset_postdata();
		?>
    </div>
</section>

<?php the_posts_navigation(); ?>
