<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
'lib/assets.php',    // Scripts and stylesheets
'lib/extras.php',    // Custom functions
'lib/setup.php',     // Theme setup
'lib/titles.php',    // Page titles
'lib/wrapper.php',   // Theme wrapper class
'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
	if (!$filepath = locate_template($file)) {
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
	}

	require_once $filepath;
}
unset($file, $filepath);

// ! Custom Happy Hippopotamus
// ini_set( 'max_file_uploads' , '5000M' );
// ini_set( 'upload_max_size' , '5000M' );
// ini_set( 'post_max_size', '5000M');
// ini_set( 'max_execution_time', '300' );

add_action( 'init', 'stop_heartbeat', 1 );
function stop_heartbeat() {
	wp_deregister_script('heartbeat');
}

add_filter( 'wp_image_editors', 'change_graphic_lib' );
function change_graphic_lib($array) {
return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

// abbreviate view counts above 1000 as 1K
function cp_convert_big_number($number) {
	$number = str_replace(',', '', $number);
	if ( $number >= 1000 ) {
		$number = number_format($number / 1000, 1) . 'K';
	} else if ( $number >= 1000000 ) {
		$number = number_format($number / 1000000, 1) . 'M';
	} else if ( $number >= 1000000000 ) {
		$number = number_format($number / 1000000000, 1) . 'B';
	}		
	return $number;
} add_filter('number_format_i18n', 'cp_convert_big_number');

//unset all those WPSEO shizzle  columns
function rkv_remove_columns( $columns ) {
	unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-links'] );
	unset( $columns['wpseo-focuskw'] );
	return $columns;
}
add_filter ( 'manage_edit-post_columns', 'rkv_remove_columns' );

// Register Custom Navigation Walker
// https://github.com/wp-bootstrap/wp-bootstrap-navwalker

require_once('wp-bootstrap-navwalker.php');

register_nav_menus( array(
		'account' => __( 'Account Dropdown', 'zoereel' ),
		'browsed' => __( 'Browse Dropdown', 'zoereel' ),
		'browsem' => __( 'Browse Mobile', 'zoereel' ),
	) );

// Admin styles: https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
function load_custom_wp_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

// if (is_single()) {
function load_mediaelement_video() {
	wp_enqueue_script( 'wp-mediaelement' );
	wp_enqueue_style( 'wp-mediaelement' );
}

add_action( 'wp_enqueue_scripts', 'load_mediaelement_video' );
// }

function zoereel_menus() {
	register_nav_menus(
		array(
			// 'another-menu' => __( 'Another Menu' ),
			'mobile-menu' => __( 'Mobile Menu' )
		)
	);
}
add_action( 'init', 'zoereel_menus' );

// unset($profile_fields['aim']);
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

// TODO: Check if this filter has any use
// function posts_for_current_author($query) {
// 	global $pagenow;
// 
// 	if( 'edit.php' != $pagenow || !$query->is_admin )
// 		return $query;
// 
// 	if( !current_user_can( 'edit_others_posts' ) ) {
// 		global $user_ID;
// 		$query->set('author', $user_ID );
// 	}
// 	return $query;
// }
// add_filter('pre_get_posts', 'posts_for_current_author');

// Filter to fix the Post Author Dropdown
// https://wp-types.com/forums/topic/manually-created-user-roles-not-showing-in-author-dropdown/
add_filter('wp_dropdown_users', 'theme_post_author_override');
function theme_post_author_override($output)
{
    global $post, $user_ID;
     
  // return if this isn't the theme author override dropdown
  if (!preg_match('/post_author_override/', $output)) return $output;
 
  // return if we've already replaced the list (end recursion)
  if (preg_match ('/post_author_override_replaced/', $output)) return $output;
 
  // replacement call to wp_dropdown_users
    $output = wp_dropdown_users(array(
      'echo' => 0,
        'name' => 'post_author_override_replaced',
        'selected' => empty($post->ID) ? $user_ID : $post->post_author,
        'include_selected' => true,
        'role__in' => ['artist','studio','admin','editor']
    ));
 
    // put the original name back
    $output = preg_replace('/post_author_override_replaced/', 'post_author_override', $output);
 
  return $output;
}

set_post_thumbnail_size( 190, 190, array( 'center', 'center')  );

// function inquireArtist(){
//
//  // $userID = get_current_user_id();
//  // um_fetch_user( $userID );
// //  if (um_user('role') === 'studio'){
// //    echo '<!--placeholder inquire button popup form in modal --><span id='inquire" class="btn btn-secondary">Inquire</span>';
// //   }
// }
// add_action( 'um_profile_header', 'inquireArtist', 20 );

add_filter( 'body_class','add_role_class' );
function add_role_class( $classes ) {
	$classes[] = um_user('role');
	return $classes;
}

function filter_w_comments( $where = '' ) {
    // $where .= " AND comment_count > 0 ";
    $where .= " AND comment_count > reeltalk_count ";
    return $where;
}
function filter_w_reeltalk( $where = '' ) {
    $where .= " AND reeltalk_count > 0 ";
    return $where;
}


//! Add Playlist & Reeltalk Tab to User Profile
// http://docs.ultimatemember.com/article/64-extending-ultimate-member-profile-menu-using-hooks
//! Hide profile tabs from other user roles
//  http://docs.ultimatemember.com/article/204-hide-profile-tabs-from-other-user-roles
/* First we need to extend main profile tabs */

add_filter('um_profile_tabs', 'add_playlist_profile_tab', 1000 );
function add_playlist_profile_tab( $tabs ) {

	$user_id = um_get_requested_user();
	$hide_from_roles = array( 'audience','member');
	if ( is_user_logged_in() && ! in_array( um_user('role') , $hide_from_roles )  ) {
		$tabs['reeltalk'] = array(
			'name' => 'ReelTalk History',
			'icon' => 'um-faicon-comments-o',
		);
	}
	$tabs['playlist'] = array(
		'name' => 'My List',
		'icon' => 'um-faicon-th-list',
	);
	unset($tabs['messages']);

	return $tabs;

}

/* Then we just have to add content to that tab using this action */

add_action('um_profile_content_reeltalk_default', 'um_profile_content_reeltalk_default');
function um_profile_content_reeltalk_default( $args ) {
	global $ultimatemember;
	$ultimatemember->shortcodes->load_template('profile/reeltalk-single');
}

add_action('um_profile_content_playlist_default', 'um_profile_content_playlist_default');
function um_profile_content_playlist_default( $args ) {
	echo do_shortcode('[cbxwpbookmark userid="'.um_user("ID").'"]');
}

/**
 // ! Rename "Posts" to "Videos"
 */
add_action( 'admin_menu', 'change_post_menu_label' );
add_action( 'init', 'change_post_object_label' );
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Videos';
	$submenu['edit.php'][5][0] = 'Videos';
	$submenu['edit.php'][10][0] = 'Add Video';
	$submenu['edit.php'][16][0] = 'Video Tags';
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Videos';
	$labels->singular_name = 'Video';
	$labels->add_new = 'Add Video';
	$labels->add_new_item = 'Add Video';
	$labels->edit_item = 'Edit Video';
	$labels->new_item = 'Video';
	$labels->view_item = 'View Video';
	$labels->search_items = 'Search Videos';
	$labels->not_found = 'No Video found';
	$labels->not_found_in_trash = 'No Video found in Trash';
}

// define the wp_update_comment_data callback https://developer.wordpress.org/reference/hooks/wp_update_comment_data/
// function add_reeltalk_type( $data ) {
//     $data['comment_type'] = 'reeltalk';
//     return $data;
// }
// add_filter( 'wp_update_comment_data', 'add_reeltalk_type', 10, 3 );

// https://codex.wordpress.org/Plugin_API/Filter_Reference/preprocess_comment
function preprocess_comment_handler( $commentdata ) {
	if ( ( isset( $_POST['commenttype'] ) ) && ( $_POST['commenttype'] != '') ) {
		// $commenttype = wp_filter_nohtml_kses($_POST['commenttype']);
		$commentdata['comment_type'] = 'reeltalk';
	}
	return $commentdata;
}
add_filter( 'preprocess_comment' , 'preprocess_comment_handler' );

// https://codex.wordpress.org/Function_Reference/wp_list_comments - displaying comments
function format_comment($comment, $args, $depth) {

// echo '<pre>';
// print_r ($comment);
// echo '</pre>';
global $ultimatemember;
um_fetch_user( $comment->user_id );
    if ( 'reeltalk' === $args['comment_type'] ) {
        $replyID = 'reeltalkFormWrap';
    } else {
        // $replyID = 'commentFormWrap';
        $replyID = 'respond';
    }    
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li ';
        $add_below = 'div-comment';
    }?>
    <<?php echo $tag; comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID() ?>"><?php 
    if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
    } ?>
        <div class="comment-author vcard um-role-<?php echo $ultimatemember->user->get_role(); ?>"><?php 
            if ( $args['avatar_size'] != 0 ) {
                echo get_avatar( $comment->comment_author_email, $args['avatar_size'] );
            } 
            printf( __( '<cite class="fn">%s</cite> ' ), get_comment_author_link() ); ?>
        </div><?php 
        if ( $comment->comment_approved == '0' ) { ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br/><?php 
        } ?>
        <div class="comment-meta commentmetadata">
            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php
	            echo sprintf(__('%s ago','ultimate-member'), human_time_diff( get_comment_date('U'), current_time('timestamp') ) );
	            
                /* translators: 1: date, 2: time */
                // printf( 
                //     __('%1$s at %2$s'), 
                //     get_comment_date(),  
                //     get_comment_time() 
                // ); ?>
            </a><?php 
            edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
        </div>

        <?php comment_text(); ?>

        <div class="reply"><?php 
	        	echo get_comment_reply_link(
                // comment_reply_link( 
                    array_merge( 
                        $args, 
                        array( 
                            'add_below' => $add_below, 
                            'respond_id' => $replyID,
                            'depth'     => $depth, 
                            'max_depth' => $args['max_depth'] 
                        ) 
                    ) 
                ); ?>
        </div><?php 
    if ( 'div' != $args['style'] ) { ?>
        </div><?php 
    }
}

// ! wpDiscuz: Ultimate Member Profile Display Name Integration
// /wp/wp-admin/edit-comments.php?page=wpdiscuz_options_page#optionsTab7|integrationsChild4
// add_filter('wpdiscuz_comment_author', 'wpdiscuz_um_author', 10, 2);
// function wpdiscuz_um_author($author_name, $comment) {
//     if ($comment->user_id) {
//         $column = 'display_name'; // Other options: 'user_login', 'user_nicename', 'nickname', 'first_name', 'last_name'
//         if (class_exists('UM_API')) {
//             um_fetch_user($comment->user_id); $author_name = um_user($column); um_reset_user();
//         } else {
//             $author_name = get_the_author_meta($column, $comment->user_id);
//         }
//     }
//     return $author_name;
// }

// ! wpDiscuz: Ultimate Member Profile URL Integration

// add_filter('wpdiscuz_profile_url', 'wpdiscuz_um_profile_url', 10, 2);
// function wpdiscuz_um_profile_url($profile_url, $user) {
//     if ($user && class_exists('UM_API')) {
//         um_fetch_user($user->ID); $profile_url = um_user_profile_url();
//     }
//     return $profile_url;
// }

// add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

function my_deregister_styles() {
	wp_deregister_style( 'wp-admin' );
}

// ! Create Video Upload form
$videoUpload = array(
	'id' => 'video_upload',
	/* (int|string) The post ID to load data from and save data to. Defaults to the current post ID.
	Can also be set to 'new_post' to create a new post on submit */
	// 'post_id' => false,
	'post_id' => 'new_post',
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	The above 'post_id' setting must contain a value of 'new_post' */
	// 'new_post' => false,
	'new_post' => true,
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	/* (boolean) Whether or not to show the post title text field. Defaults to false */
	'post_title' => true,
	/* (boolean) Whether or not to show the post content editor field. Defaults to false */
	// 'post_content' => false,
	'post_content' => true,
	/* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	'form' => true,
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '/video-submitted-confirmation?v=%post_id%',
	// 'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit", 'acf'),
	'updated_message' => __("Video updated", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use the WP uploader or a basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic' */
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	// 'html_update_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $videoUpload );

// ! Create Trailer For Sale Upload form
$videoUpload = array(
	'id' => 'trailer_upload_for_sale',
	'post_id' => 'new_post',
	'new_post' => true,
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => true,
	'form' => true,
	'form_attributes' => array(),
	'return' => '/submission-confirmation?v=%post_id%',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit", 'acf'),
	'updated_message' => __("Submission Updated", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $videoUpload );

// https://gist.github.com/anonymous/18229d7dd19d065aa1e9
// set featured image from ACF image upload
function acf_set_featured_image( $value, $post_id, $field  ){
	if($value != ''){
		//Add the value which is the image ID to the _thumbnail_id meta data for the current post
		add_post_meta($post_id, '_thumbnail_id', $value);
	}
	return $value;
}
add_filter('acf/update_value/name=cover_art', 'acf_set_featured_image', 10, 3);

// ! Register Inquiry Form
$inquiry = array(
	'id' => 'project_inquiry',
	'post_id' => 'new_post',
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_type' => 'project_inquiry',
		'post_status'   => 'publish'
	),
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true,
	'form_attributes' => array(),
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit Inquiry", 'acf'),
	'updated_message' => __("Update Inquiry", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $inquiry );

// ! Register Job Posting Form
$posting = array(
	'id' => 'job_posting',
	'post_id' => 'new_post',
	'new_post' => array(
		'post_type' => 'posting',
		'post_status'   => 'publish'
	),
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true,
	'form_attributes' => array(),
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit Job Posting", 'acf'),
	'updated_message' => __("Update Job Posting", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>Posting Updated</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $posting );

// ! Register Submission Form
$submission = array(
	'id' => 'submission',
	'post_id' => 'new_post',
	'new_post' => array(
		'post_title' => 'Submission',
		'post_type' => 'submission',
		'post_status'   => 'publish'
	),
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true,
	'form_attributes' => array(),
	'return' => '%post_url%',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Send Submission", 'acf'),
	'updated_message' => __("Update Submission", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $submission );

// ! Register Submission Form
$survey = array(
	'id' => 'survey',
	'post_id' => 'new_post',
	'new_post' => array(
		'post_type' => 'survey',
		'post_status'   => 'publish'
	),
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true,
	'form_attributes' => array(),
	'return' => '%post_url%&submitted=%post_id%',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Send", 'acf'),
	'updated_message' => __("Update Survey", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $survey );

// ! Register Strike A Deal Form
$deal = array(
	'id' => 'strike_deal',
	'post_id' => 'new_post',
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_type' => 'deals',
		'post_status'   => 'publish'
	),
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true,
	'form_attributes' => array(),
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Strike a Deal & Contact Seller", 'acf'),
	'updated_message' => __("Update Deal", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message' => '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button' => '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner' => '<span class="acf-spinner"></span>'
);
acf_register_form( $deal );

function list_jobs_in_post( $field ) {
	if (isset($field['choices'][0])) { return; }
    // reset choices
    $field['choices'] = array(); $i=0;

    $jobpost = get_the_ID();
    $rows = get_field('production_jobs', $jobpost);
    if($rows) {
	    foreach($rows as $row) {
		    $job = $row['job_postion'];
	    	$field['choices'][$job] = $job;
	    	$i++;
	    }
    }
    return $field;
}
add_filter('acf/load_field/name=job_applying_for', 'list_jobs_in_post');

function get_users_videos( $field ) {
	if (isset($field['choices'][0])) { return; }
    // reset choices
    $field['choices'] = array(); $i=0;

    $author_query = array('posts_per_page' => '-1','author' => get_current_user_id());
    $author_posts = new WP_Query($author_query);
    
    while($author_posts->have_posts()) : $author_posts->the_post();
      $field['choices'][get_the_permalink()] = get_the_title();
      $i++;
    endwhile;
    wp_reset_postdata();
    return $field;
}
add_filter('acf/load_field/name=link_to_reel', 'get_users_videos');

function submission_for_job_post( $value, $post_id, $field ) {
	$value = get_the_ID();
	return $value;
}
add_filter('acf/load_value/name=job_posting', 'submission_for_job_post', 10, 3);

function deal_for_movie( $value, $post_id, $field ) {
	$value = get_the_ID();
	return $value;
}
add_filter('acf/load_value/name=movie_dealing', 'deal_for_movie', 10, 3);

function survey_for_video( $value, $post_id, $field ) {
	$value = get_the_ID();
	return $value;
}
add_filter('acf/load_value/name=video_id', 'survey_for_video', 10, 3);

function survey_submission_artist_email( $value, $post_id, $field ) {
	$user_id = get_post_field( 'post_author', $post_id );
	$user_info = get_userdata($user_id);
	$value = $user_info->user_email; 
	return $value;
}
add_filter('acf/load_value/name=video_artist_email', 'survey_submission_artist_email', 10, 3);

function survey_submission_artist_name( $value, $post_id, $field ) {
	$user_id = get_post_field( 'post_author', $post_id );
	$user_info = get_userdata($user_id);
	$value = $user_info->first_name; 
	return $value;
}
add_filter('acf/load_value/name=video_artist_name', 'survey_submission_artist_name', 10, 3);

function survey_submission_surveyors_profile_url( $value, $post_id, $field ) {
	$value = um_user_profile_url(); 
	return $value;
}
add_filter('acf/load_value/name=surveyors_profile_url', 'survey_submission_surveyors_profile_url', 10, 3);

function upload_for_sale_viewing( $value, $post_id, $field ) {
	if (is_page(483)) {
		$value = 'studios';
	}
	return $value;
}
add_filter('acf/load_value/name=viewing_permissions', 'upload_for_sale_viewing', 10, 3);

// function my_acf_pre_save_post( $post_id, $form ) {
//
//  // create post using $form and update $post_id
// error_log(print_r( $post_id, true ), 3, '/Users/Gianna/Sites/valet/zoereel/printout.log');
// error_log(print_r( $form, true ), 3, '/Users/Gianna/Sites/valet/zoereel/printout.log');
//  // return
//  return $post_id;
// }
//
// add_filter('acf/pre_save_post', 'my_acf_pre_save_post', 10, 2);

// add_action('acf/save_post', 'video_post');
//
// function video_post( $post_id ) {
//
//  // bail early if editing in admin
//  if( is_admin() ) {
//   return;
//  }
//
//  // vars
//  $post = get_post( $post_id );
//
//  // get custom fields (field group exists for content_form)
//  $video = get_field('video', $post_id);
//  $crew1 = get_field('crew_1', $post_id);
//  $crew1n = get_field('crew_1_n', $post_id);
//
//
//  // email data
//  $to = 'david@prideandpixel.com';
//  $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
//  $subject = $post->post_title;
//  $body = $post->post_content;
//
//  // send email
//  wp_mail($to, $subject, $body, $headers );
// }


// ! Create Inquiry Custom Post
// https://codex.wordpress.org/Function_Reference/register_post_type
// Post Type created http://fooplugins.com/generators/wordpress-custom-post-type-code-output/?id=12958
add_action( 'init', 'register_cpts' );

function register_cpts() {

	$PIlabels = array(
		'name' => __( 'Project Inquiries', 'zoereel' ),
		'singular_name' => __( 'Project Inquiry', 'zoereel' ),
		'add_new' => __( 'Add New Inquiry', 'zoereel' ),
		'add_new_item' => __( 'Add New Project Inquiry', 'zoereel' ),
		'edit_item' => __( 'Edit Inquiry', 'zoereel' ),
		'new_item' => __( 'New Project Inquiry', 'zoereel' ),
		'view_item' => __( 'View Inquiry', 'zoereel' ),
		'search_items' => __( 'Search Project Inquiries', 'zoereel' ),
		'not_found' => __( 'No project inquiries found', 'zoereel' ),
		'not_found_in_trash' => __( 'No project inquiries found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Project Inquiry:', 'zoereel' ),
		'menu_name' => __( 'Project Inquiries', 'zoereel' ),
	);

	$PIargs = array(
		'labels' => $PIlabels,
		'hierarchical' => false,
		'description' => 'Inquiries of projects',
		'supports' => array( 'title','author', 'thumbnail' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);

	register_post_type( 'project_inquiry', $PIargs );

// 	$JPlabels = array(
// 		'name' => __( 'Job Postings', 'zoereel' ),
// 		'singular_name' => __( 'Job Posting', 'zoereel' ),
// 		'add_new' => __( 'Add Job Posting', 'zoereel' ),
// 		'add_new_item' => __( 'Add New Job Posting', 'zoereel' ),
// 		'edit_item' => __( 'Edit Inquiry', 'zoereel' ),
// 		'new_item' => __( 'New Job Posting', 'zoereel' ),
// 		'view_item' => __( 'View Posting', 'posting' ),
// 		'search_items' => __( 'Search Job Postings', 'zoereel' ),
// 		'not_found' => __( 'No job postings found', 'zoereel' ),
// 		'not_found_in_trash' => __( 'No job postings found in Trash', 'zoereel' ),
// 		'parent_item_colon' => __( 'Parent Job Posting:', 'zoereel' ),
// 		'menu_name' => __( 'Job Postings', 'zoereel' ),
// 	);
// 
// 	$JPargs = array(
// 		'labels' => $JPlabels,
// 		'hierarchical' => false,
// 		'description' => 'Job Postings',
// 		'supports' => array( 'title','author' ),
// 		'taxonomies' => array( 'category', 'post_tag' ),
// 		'public' => false,
// 		'show_ui' => true,
// 		'show_in_menu' => true,
// 		'menu_position' => 7,
// 		'show_in_nav_menus' => false,
// 		'publicly_queryable' => true,
// 		'exclude_from_search' => true,
// 		'has_archive' => false,
// 		'query_var' => true,
// 		'can_export' => true,
// 		// 'rewrite' => false,
// 		'rewrite' => true,
// 		// 'capability_type' => 'inquiries'
// 		'capability_type' => 'post'
// 	);
// 
// 	register_post_type( 'posting', $JPargs );

	$SUBlabels = array(
		'name' => __( 'Job Submissions', 'zoereel' ),
		'singular_name' => __( 'Job Submission', 'zoereel' ),
		'menu_name' => __( 'Submissions', 'zoereel' ),
		'all_items' => __( 'All Submissions', 'zoereel' ),		
		'add_new' => __( 'Add New', 'zoereel' ),
		'add_new_item' => __( 'Add New Submission', 'zoereel' ),
		'edit_item' => __( 'Edit Submission', 'zoereel' ),
		'new_item' => __( 'New Submission', 'zoereel' ),
		'view_item' => __( 'View Submission', 'zoereel' ),
		'view_items' => __( 'View Submission', 'zoereel' ),		
		'search_items' => __( 'Search Submissions', 'zoereel' ),
		'not_found' => __( 'No submissions found', 'zoereel' ),
		'not_found_in_trash' => __( 'No submissions found in Trash', 'zoereel' ),
		'archives' => __( 'Job Submissions Archives', 'zoereel' ),
		'insert_into_item' => __( 'Insert into submissions', 'zoereel' ),
		'uploaded_to_this_item' => __( 'Uploaded to this submissions', 'zoereel' ),
		'filter_items_list' => __( 'Filter submissions', 'zoereel' ),
		'items_list_navigation' => __( 'Submission list navigation', 'zoereel' ),
		'items_list' => __( 'Submissions List', 'zoereel' ),
		'attributes' => __( 'Submission Attributes', 'zoereel' ),
	);

	$SUBargs = array(
		'label' => __( 'Job Submissions', 'zoereel' ),		
		'labels' => $SUBlabels,
		'hierarchical' => false,
		'description' => 'Submissions from Job Postings',
		'supports' => array( 'title','author' ),
		// 'taxonomies' => array( 'category', 'post_tag' ),

		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_rest' => false,
		'rest_base' => '',
		'has_archive' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'capability_type' => 'post',
		// 'map_meta_cap' => true, // check further https://codex.wordpress.org/Function_Reference/register_post_type
		'rewrite' => false,
		'query_var' => true,
		'menu_position' => 6,
		// 'can_export' => true,
	);
	// Post Type created http://fooplugins.com/generators/wordpress-custom-post-type-code-output/?id=13064
	register_post_type( 'submission', $SUBargs );

	$ASlabels = array(
		'name' => __( 'Audience Surveys', 'zoereel' ),
		'singular_name' => __( 'Survey', 'zoereel' ),
		'menu_name' => __( 'Surveys', 'zoereel' ),
		'all_items' => __( 'All Surveys', 'zoereel' ),
		'add_new' => __( 'Add Audience Survey', 'zoereel' ),
		'add_new_item' => __( 'Add New Survey', 'zoereel' ),
		'edit_item' => __( 'Edit Survey', 'zoereel' ),
		'new_item' => __( 'New Survey', 'zoereel' ),
		'view_item' => __( 'View Survey', 'zoereel' ),
		'view_items' => __( 'View Audience Surveys', 'zoereel' ),
		'search_items' => __( 'Search Audience Surveys', 'zoereel' ),
		'not_found' => __( 'This search was not found in any of our surveys', 'zoereel' ),
		'not_found_in_trash' => __( 'No Surveys found in trash', 'zoereel' ),
		'archives' => __( 'Audience Survey Archives', 'zoereel' ),
		'insert_into_item' => __( 'Insert into survey', 'zoereel' ),
		'uploaded_to_this_item' => __( 'Uploaded to this survey', 'zoereel' ),
		'filter_items_list' => __( 'Filter surveys', 'zoereel' ),
		'items_list_navigation' => __( 'Survey list navigation', 'zoereel' ),
		'items_list' => __( 'Survey List', 'zoereel' ),
		'attributes' => __( 'Survey Attributes', 'zoereel' ),
	);

	$ASargs = array(
		'label' => __( 'Audience Surveys', 'zoereel' ),
		'labels' => $ASlabels,
		'hierarchical' => false,		
		'description' => 'Audience surveys of video content for artists and studios',
		'supports' => array( 'author','title' ),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_rest' => false,
		'rest_base' => '',
		'has_archive' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'capability_type' => 'post',
		// 'map_meta_cap' => true,
		'rewrite' => false,
		'query_var' => true,
		'menu_position' => 6,
	);

	register_post_type( 'survey', $ASargs );

	$Deallabels = array(
		'name' => __( 'Deals', 'zoereel' ),
		'singular_name' => __( 'Deal', 'zoereel' ),
		'add_new' => __( 'Add New', 'zoereel' ),
		'add_new_item' => __( 'Add New Deal', 'zoereel' ),
		'edit_item' => __( 'Edit Deal', 'zoereel' ),
		'new_item' => __( 'New Deal', 'zoereel' ),
		'view_item' => __( 'View Deal', 'zoereel' ),
		'search_items' => __( 'Search Deals', 'zoereel' ),
		'not_found' => __( 'No deals found', 'zoereel' ),
		'not_found_in_trash' => __( 'No deals found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Deal:', 'zoereel' ),
		'menu_name' => __( 'Deals', 'zoereel' ),
	);

	$Dealargs = array(
		'labels' => $Deallabels,
		'hierarchical' => false,
		'description' => 'Movie Deals',
		'menu_icon'   => 'dashicons-tickets-alt',
		'supports' => array( 'title', 'author', 'comments' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);
	register_post_type( 'deals', $Dealargs );
}

// ! Register Genre Taxonomy
// created from https://generatewp.com/taxonomy/?clone=music-genre-taxonomy
function genre_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Genres', 'Taxonomy General Name', 'zoereel' ),
		'singular_name'              => _x( 'Genre', 'Taxonomy Singular Name', 'zoereel' ),
		'menu_name'                  => __( 'Genre', 'zoereel' ),
		'all_items'                  => __( 'All Genres', 'zoereel' ),
		'parent_item'                => __( 'Parent Genre', 'zoereel' ),
		'parent_item_colon'          => __( 'Parent Genre:', 'zoereel' ),
		'new_item_name'              => __( 'New Genre Name', 'zoereel' ),
		'add_new_item'               => __( 'Add New Genre', 'zoereel' ),
		'edit_item'                  => __( 'Edit Genre', 'zoereel' ),
		'update_item'                => __( 'Update Genre', 'zoereel' ),
		'view_item'                  => __( 'View Item', 'zoereel' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'zoereel' ),
		'add_or_remove_items'        => __( 'Add or remove genres', 'zoereel' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'zoereel' ),
		'popular_items'              => __( 'Popular Items', 'zoereel' ),
		'search_items'               => __( 'Search genres', 'zoereel' ),
		'not_found'                  => __( 'Not Found', 'zoereel' ),
		'no_terms'                   => __( 'No items', 'zoereel' ),
		'items_list'                 => __( 'Items list', 'zoereel' ),
		'items_list_navigation'      => __( 'Items list navigation', 'zoereel' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'genre', array( 'post' ), $args );

}
add_action( 'init', 'genre_taxonomy', 0 );

/*
http://docs.ultimatemember.com/article/53-using-notifications-api-to-add-custom-notifications

This code sample shows you how to use the API to create
and add custom notifications (for real-time notifications)
plugin.

STEP 1: You need to extend the filter: um_notifications_core_log_types with your
new notification type as follows for example

*/

add_filter('um_notifications_core_log_types', 'add_survey_view_notification_type', 200 );
function add_survey_view_notification_type( $array ) {
		
	$array['survey_view'] = array(
		'title' => 'When someone views a survey', // title for reference in backend settings
		'template' => '<strong>{member}</strong> has just viewed your survey.', // the template, {member} is a tag, this is how the notification will appear in your notifications
		'account_desc' => 'When member does some action on my profile', // title for account page (notification settings)
	);
		
	return $array;
}

/*

STEP 2: Add an icon and color to this new notification type

*/

add_filter('um_notifications_get_icon', 'add_survey_view_notification_icon', 10, 2 );
function add_survey_view_notification_icon( $output, $type ) {
	
	if ( $type == 'survey_view' ) { // note that our new action id is "survey_view" from previous filter
		$output = '<i class="um-icon-android-person-add" style="color: #336699"></i>';
	}
	
	return $output;
}

/*

STEP 3: Now you just need to add the notification trigger when a user does some action on
another user profile, I assume you can trigger that in some action hooks
for example when user view another user profile you can hook like this

basically you need to run this in code

$who_will_get_notification : is user ID who will get notification
'survey_view' is our new notification type
$vars is array containing the required template tags, user photo and url when that notification is clicked

$um_notifications->api->store_notification( $who_will_get_notification, 'survey_view', $vars );

*/

add_action('notify_survey_view', 'trigger_survey_view_notification', 100);
function trigger_survey_view_notification( $authorID ) {
	global $um_notifications;
		
	if ( is_user_logged_in() && get_current_user_id() != $authorID ) {
			
		um_fetch_user( get_current_user_id() );
			
		$vars['photo'] = um_get_avatar_url( get_avatar( get_current_user_id(), 40 ) );
		$vars['member'] = um_user('display_name');
		$vars['notification_uri'] = um_user_profile_url();

		$um_notifications->api->store_notification( $authorID, 'survey_view', $vars );
		
	}

}

add_shortcode('zoereel-mycat', 'zoereel_shortcode_mycat');

function zoereel_shortcode_mycat($attr) { // rewrite of cbxbookmarkmypost_shortcode_mycat in /plugins/cbxwpbookmark/public/class-cbxwpbookmark-public.php

	$attr = shortcode_atts(
		array(
			'userid'     => get_current_user_id(),
			'order'      => "asc", //possible values  title, id
			'orderby'    => "cat_name",
			'privacy'    => 2,
			'show_count' => 0,
			'display'    => 0,  //0 = list  1= dropdown,
			'allowedit'  => 0
		), $attr
	);
	
	$output = (intval($attr['display']) == 0) ? '<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select<span class="caret"></span></button> <ul role="menu" class="dropdown-menu cbxbookmark-category-list cbxbookmark-category-list-sc">' : '';
	$output .= cbxbookmarkmycat_html($attr);
	$output .= (intval($attr['display']) == 0) ? '</ul></div>' : '';
	
	return $output;

}

function prefix_movie_rewrite_rule() {
    add_rewrite_rule( '([^/]+)/surveys', 'index.php?pagename=$matches[1]&surveys=yes', 'top' );
    // add_rewrite_rule( 'video/([^/]+)/videos', 'index.php?movie=$matches[1]&videos=yes', 'top' );
}
 
add_action( 'init', 'prefix_movie_rewrite_rule' );

function prefix_register_query_var( $vars ) {
	$vars[] = 'forsale';
	$vars[] = 'surveys';
	return $vars;
}
add_filter( 'query_vars', 'prefix_register_query_var' );

function prefix_url_rewrite_templates() {
 
    if ( get_query_var( 'surveys' ) ) {
        add_filter( 'template_include', function() {
            return get_template_directory() . '/single-video-surveys.php';
        });
    }
}
// https://zoereel.dev/?p=700&surveys=yes
add_action( 'template_redirect', 'prefix_url_rewrite_templates' );


// https://code.tutsplus.com/articles/custom-page-template-page-based-on-url-rewrite--wp-30564
// TOFIX Improve sale url rewrite for query variable: https://premium.wpmudev.org/blog/building-customized-urls-wordpress/
// https://kinsta.com/blog/wordpress-permalinks-url-rewriting/
function forsale_rewrite_rule() {
	// for_sale ex. https://zoereel.dev/index.php?p=52&for_sale=true
	// for_sale ex. https://zoereel.dev/movie-for-sale/52/?forsale=true
	add_rewrite_rule( 'movie-for-sale/([^/]+)', 'index.php?p=$matches[1]', 'top' );
}
add_action( 'init', 'forsale_rewrite_rule' );


// function append_query_string( $url, $post, $leavename=true ) {
//  if ( is_page( 360 ) ) {
//  // if ( $post->post_type == 'post' ) {
//   $url = $_SERVER['REQUEST_URI'] . $post->post_name;
//   $url = add_query_arg( 'sale', 'true', $url );
//  }
//  return $url;
// }
// add_filter( 'post_link', 'append_query_string', 10, 3 );


// flush_rewrite_rules();
function create_forsale_url_querystring() {
	//     add_rewrite_rule(
	//         '^movie-for-sale/([^/]*)$',
	//         'index.php?forsale=$matches[1]',
	//         'top'
	//     );
	//     add_rewrite_rule(
	//      '^movie-for-sale/([0-9]+)/([^/]*)/?',
	//      'index.php?p=$matches[1]&forsale=$matches[2]',
	//      'top'
	//     );
	add_rewrite_rule(
		'^movie-for-sale/([0-9]+)/?',
		'index.php?p=$matches[1]',
		'top'
	);

	add_rewrite_tag('%forsale%','([^/]*)');
}
// add_action( 'init', 'create_forsale_url_querystring', 10, 0);

// // make Videos hierarchical for episodic videos
// add_action('registered_post_type', 'igy2411_make_posts_hierarchical', 10, 2 );
//
// // Runs after each post type is registered
// function igy2411_make_posts_hierarchical($post_type, $pto){
//     // Return, if not post type posts
//     if ($post_type != 'post') return;
//     // access $wp_post_types global variable
//     global $wp_post_types;
//     // Set post type "post" to be hierarchical
//     $wp_post_types['post']->hierarchical = 1;
//     // Add page attributes to post backend
//     // This adds the box to set up parent and menu order on edit posts.
//     add_post_type_support( 'post', 'page-attributes' );
//
// }
// add_filter( 'page_attributes_dropdown_pages_args', 'mwm_show_only_author_pages_in_attributes' );
// add_filter( 'quick_edit_dropdown_pages_args', 'mwm_show_only_author_pages_in_attributes' );
//
// function mwm_show_only_author_pages_in_attributes( $args ) {
//     global $post;
//     $args['author'] = $post -> post_author;
//     return $args;
// }

/**
 // ! Custom Happy Hippopotamus Not In Use
 **
 **
 *************/
