<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// ! Custom Happy Hippopotamus

// Register Custom Navigation Walker
// https://github.com/wp-bootstrap/wp-bootstrap-navwalker

require_once('wp-bootstrap-navwalker.php');

register_nav_menus( array(
        'account' => __( 'Account Dropdown', 'zoereel' ),
        'browsed' => __( 'Browse Dropdown', 'zoereel' ),
        'browsem' => __( 'Browse Mobile', 'zoereel' ),        
) );

// Admin styles: https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
function load_custom_wp_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

// if (is_single()) {
	function load_mediaelement_video() {
		wp_enqueue_script( 'wp-mediaelement' );
		wp_enqueue_style( 'wp-mediaelement' );
	}
	
	add_action( 'wp_enqueue_scripts', 'load_mediaelement_video' );
// }

function zoereel_menus() {
  register_nav_menus(
    array(
      // 'another-menu' => __( 'Another Menu' ),
      'mobile-menu' => __( 'Mobile Menu' )      
    )
  );
}
add_action( 'init', 'zoereel_menus' );

// unset($profile_fields['aim']);
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

// TODO: Check if this filter has any use
function posts_for_current_author($query) {
	global $pagenow;

	if( 'edit.php' != $pagenow || !$query->is_admin )
	    return $query;

	if( !current_user_can( 'edit_others_posts' ) ) {
 	global $user_ID;
 	$query->set('author', $user_ID );
 }
	return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

set_post_thumbnail_size( 190, 190, array( 'center', 'center')  );

// function inquireArtist(){
// 
// 	// $userID = get_current_user_id();
// 	// um_fetch_user( $userID );
// // 	if (um_user('role') === 'studio'){
// //  		echo '<!--placeholder inquire button popup form in modal --><span id="inquire" class="btn btn-secondary">Inquire</span>';
// //  	}
// }
// add_action( 'um_profile_header', 'inquireArtist', 20 );

add_filter( 'body_class','add_role_class' );
function add_role_class( $classes ) {
    $classes[] = um_user('role');
    return $classes;
}


//! Add Playlist & Reeltalk Tab to User Profile
// http://docs.ultimatemember.com/article/64-extending-ultimate-member-profile-menu-using-hooks
//! Hide profile tabs from other user roles
//  http://docs.ultimatemember.com/article/204-hide-profile-tabs-from-other-user-roles
/* First we need to extend main profile tabs */

add_filter('um_profile_tabs', 'add_playlist_profile_tab', 1000 );
function add_playlist_profile_tab( $tabs ) {
	
	$user_id = um_get_requested_user();	
	$hide_from_roles = array( 'audience','member');
	if ( is_user_logged_in() && ! in_array( um_user('role') , $hide_from_roles )  ) {
		$tabs['reeltalk'] = array(
			'name' => 'ReelTalk History',
			'icon' => 'um-faicon-comments-o',
		);
	}	
	$tabs['playlist'] = array(
		'name' => 'My List',
		'icon' => 'um-faicon-th-list',
	);
		
	return $tabs;
		
}

/* Then we just have to add content to that tab using this action */

add_action('um_profile_content_reeltalk_default', 'um_profile_content_reeltalk_default');
function um_profile_content_reeltalk_default( $args ) {
	global $ultimatemember; 
// 	$ultimatemember->shortcodes->loop = $ultimatemember->query->make('post_type=comment&number=10&offset=0&user_id=' . um_user('ID') );
// 	
// 		if ( $ultimatemember->shortcodes->loop ) {
				
			$ultimatemember->shortcodes->load_template('profile/reeltalk-single');
	
// // 		echo '<div class="um-ajax-items seeethiswow">'.
// // 		
// // 			'<!--Ajax output-->';
// // 			
// // 			if ( count($ultimatemember->shortcodes->loop) >= 10 ) {
// // 			
// // 			echo '<div class="um-load-items">'.
// // 				'<a href="#" class="um-ajax-paginate um-button" data-hook="um_load_comments" data-args="comment,10,10,'. um_user('ID') .'">';
// // 				 _e('load more reeltalk','ultimate-member');
// // 			echo '</a>'.
// // 			'</div>';
// // 			
// // 			}
// // 			
// // 		echo '</div>';		
// 			
// 		} else {
// 	
// 			echo '<div class="um-profile-note comments"><span>';
// 			echo ( um_profile_id() == get_current_user_id() ) ? 'You have not made any reeltalk comments.' : 'This user has not made any reeltalk comments.';
// 			echo '</span></div>';
// 		
// 		}
}

add_action('um_profile_content_playlist_default', 'um_profile_content_playlist_default');
function um_profile_content_playlist_default( $args ) {
	echo do_shortcode('[cbxwpbookmark userid="'.um_user("ID").'"]');
}

/**
// ! Rename "Posts" to "Videos"
 */
add_action( 'admin_menu', 'change_post_menu_label' );
add_action( 'init', 'change_post_object_label' );
function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Videos';
	$submenu['edit.php'][5][0] = 'Videos';
	$submenu['edit.php'][10][0] = 'Add Video';
	$submenu['edit.php'][16][0] = 'Video Tags';
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Videos';
	$labels->singular_name = 'Video';
	$labels->add_new = 'Add Video';
	$labels->add_new_item = 'Add Video';
	$labels->edit_item = 'Edit Video';
	$labels->new_item = 'Video';
	$labels->view_item = 'View Video';
	$labels->search_items = 'Search Videos';
	$labels->not_found = 'No Video found';
	$labels->not_found_in_trash = 'No Video found in Trash';
}

// define the wp_update_comment_data callback https://developer.wordpress.org/reference/hooks/wp_update_comment_data/
// function add_reeltalk_type( $data ) {
//     $data['comment_type'] = 'reeltalk';
//     return $data;
// }
// add_filter( 'wp_update_comment_data', 'add_reeltalk_type', 10, 3 );

// https://codex.wordpress.org/Plugin_API/Filter_Reference/preprocess_comment
function preprocess_comment_handler( $commentdata ) {
	if ( ( isset( $_POST['commenttype'] ) ) && ( $_POST['commenttype'] != '') ) {
		// $commenttype = wp_filter_nohtml_kses($_POST['commenttype']);
		    $commentdata['comment_type'] = 'reeltalk';
	}	
    return $commentdata;
}
add_filter( 'preprocess_comment' , 'preprocess_comment_handler' );

// ! wpDiscuz: Ultimate Member Profile Display Name Integration 
// /wp/wp-admin/edit-comments.php?page=wpdiscuz_options_page#optionsTab7|integrationsChild4
// add_filter('wpdiscuz_comment_author', 'wpdiscuz_um_author', 10, 2);
// function wpdiscuz_um_author($author_name, $comment) {
//     if ($comment->user_id) {
//         $column = 'display_name'; // Other options: 'user_login', 'user_nicename', 'nickname', 'first_name', 'last_name'
//         if (class_exists('UM_API')) {
//             um_fetch_user($comment->user_id); $author_name = um_user($column); um_reset_user();
//         } else {
//             $author_name = get_the_author_meta($column, $comment->user_id);
//         }
//     }
//     return $author_name;
// }

// ! wpDiscuz: Ultimate Member Profile URL Integration 

// add_filter('wpdiscuz_profile_url', 'wpdiscuz_um_profile_url', 10, 2);
// function wpdiscuz_um_profile_url($profile_url, $user) {
//     if ($user && class_exists('UM_API')) {
//         um_fetch_user($user->ID); $profile_url = um_user_profile_url();
//     }
//     return $profile_url;
// }

// add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
 
function my_deregister_styles() {
	wp_deregister_style( 'wp-admin' );
}

// ! Create Video Upload form
$videoUpload = array(
	'id' => 'video_upload',
	/* (int|string) The post ID to load data from and save data to. Defaults to the current post ID. 
	Can also be set to 'new_post' to create a new post on submit */
	// 'post_id' => false,
	'post_id' => 'new_post',
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	The above 'post_id' setting must contain a value of 'new_post' */
	// 'new_post' => false,
	'new_post' => true,
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	/* (boolean) Whether or not to show the post title text field. Defaults to false */
	'post_title' => true,
	/* (boolean) Whether or not to show the post content editor field. Defaults to false */
	// 'post_content' => false,
	'post_content' => true,
	/* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	'form' => true,
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '/video-submitted-confirmation?v=%post_id%',
	// 'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Add Video", 'acf'),
	'updated_message' => __("Video updated", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use the WP uploader or a basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic'. Added in v5.2.4 */
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	// 'html_update_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $videoUpload );

// ! Create Trailer For Sale Upload form
$videoUpload = array(
	'id' => 'trailer_upload_for_sale',
	'post_id' => 'new_post',
	'new_post' => true,
	'field_groups' => false,
	'fields' => false,
	'post_title' => true,
	'post_content' => true,
	'form' => true,
	'form_attributes' => array(),
	'return' => '/submission-confirmation?v=%post_id%',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit", 'acf'),
	'updated_message' => __("Submission Updated", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp',
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $videoUpload );

// https://gist.github.com/anonymous/18229d7dd19d065aa1e9	
	// set featured image from ACF image upload
function acf_set_featured_image( $value, $post_id, $field  ){
	if($value != ''){
		//Add the value which is the image ID to the _thumbnail_id meta data for the current post
		add_post_meta($post_id, '_thumbnail_id', $value);
	}
	return $value;
}
add_filter('acf/update_value/name=cover_art', 'acf_set_featured_image', 10, 3);

// ! Register Inquiry Form
$inquiry = array(
	'id' => 'project_inquiry',
	'post_id' => 'new_post',	
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_type' => 'project_inquiry',
		'post_status'   => 'publish'	
	),
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true, /* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit Inquiry", 'acf'),
	'updated_message' => __("Update Inquiry", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use WP uploader or basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic' */
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $inquiry );

// ! Register Job Posting Form
$posting = array(
	'id' => 'job_posting',
	'post_id' => 'new_post',	
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_type' => 'posting',
		'post_status'   => 'publish'	
	),
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true, /* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Submit Job Posting", 'acf'),
	'updated_message' => __("Update Job Posting", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use WP uploader or basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic' */
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $posting );

// ! Register Submission Form
$submission = array(
	'id' => 'submission',
	'post_id' => 'new_post',	
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_title'	=> 'Submission',
		'post_type' => 'submission',
		'post_status'   => 'publish'	
	),
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true, /* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Send Submission", 'acf'),
	'updated_message' => __("Update Submission", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use WP uploader or basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic' */
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $submission );

// ! Register Strike A Deal Form
$deal = array(
	'id' => 'strike_deal',
	'post_id' => 'new_post',	
	/* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
	https://developer.wordpress.org/reference/functions/wp_insert_post/	*/
	'new_post' => array(
		'post_type' => 'deals',
		'post_status'   => 'publish'	
	),
	/* (array) An array of field group IDs/keys to override the fields displayed in this form */
	'field_groups' => false,
	/* (array) An array of field IDs/keys to override the fields displayed in this form */
	'fields' => false,
	'post_title' => true,
	'post_content' => false,
	'form' => true, /* (boolean) Whether or not to create a form element. Useful when a adding to an existing form. Defaults to true */
	/* (array) An array or HTML attributes for the form element */
	'form_attributes' => array(),
	/* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
	A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
	'return' => '',
	'html_before_fields' => '',
	'html_after_fields' => '',
	'submit_value' => __("Strike a Deal & Contact Seller", 'acf'),
	'updated_message' => __("Update Deal", 'acf'),
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'field_el' => 'div',
	'uploader' => 'wp', /* (string) Whether to use WP uploader or basic input for image and file fields. Defaults to 'wp' Choices of 'wp' or 'basic' */
	'honeypot' => true,
	'html_updated_message'	=> '<div id="message" class="updated"><p>%s</p></div>',
	'html_submit_button'	=> '<input type="submit" class="btn btn-secondary" value="%s" />',
	'html_submit_spinner'	=> '<span class="acf-spinner"></span>'
);
acf_register_form( $deal );

// https://www.advancedcustomfields.com/resources/acf-load_value/
function submission_for_job_post( $value, $post_id, $field ) {
    $value = get_the_ID(); 
    return $value;  
}
	add_filter('acf/load_value/name=job_posting', 'submission_for_job_post', 10, 3);
	
function deal_for_movie( $value, $post_id, $field ) {
    $value = get_the_ID(); 
    return $value;  
}
	add_filter('acf/load_value/name=movie_dealing', 'deal_for_movie', 10, 3);

// function my_acf_pre_save_post( $post_id, $form ) {
// 	
// 	// create post using $form and update $post_id
// error_log(print_r( $post_id, true ), 3, '/Users/Gianna/Sites/valet/zoereel/printout.log'); 
// error_log(print_r( $form, true ), 3, '/Users/Gianna/Sites/valet/zoereel/printout.log'); 	
// 	// return
// 	return $post_id;
// }
// 
// add_filter('acf/pre_save_post', 'my_acf_pre_save_post', 10, 2);

// add_action('acf/save_post', 'video_post');
// 
// function video_post( $post_id ) {
// 	
// 	// bail early if editing in admin
// 	if( is_admin() ) {
// 		return;	
// 	}
// 	
// 	// vars
// 	$post = get_post( $post_id );
// 		
// 	// get custom fields (field group exists for content_form)
// 	$video = get_field('video', $post_id);
// 	$crew1 = get_field('crew_1', $post_id);
// 	$crew1n = get_field('crew_1_n', $post_id);
// 	
// 	
// 	// email data
// 	$to = 'david@prideandpixel.com';
// 	$headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
// 	$subject = $post->post_title;
// 	$body = $post->post_content;
// 
// 	// send email
// 	wp_mail($to, $subject, $body, $headers );
// }


// ! Create Inquiry Custom Post
// https://codex.wordpress.org/Function_Reference/register_post_type
// Post Type created http://fooplugins.com/generators/wordpress-custom-post-type-code-output/?id=12958
add_action( 'init', 'register_cpts' );

function register_cpts() {

	$PIlabels = array(
		'name' => __( 'Project Inquiries', 'zoereel' ),
		'singular_name' => __( 'Project Inquiry', 'zoereel' ),
		'add_new' => __( 'Add New Inquiry', 'zoereel' ),
		'add_new_item' => __( 'Add New Project Inquiry', 'zoereel' ),
		'edit_item' => __( 'Edit Inquiry', 'zoereel' ),
		'new_item' => __( 'New Project Inquiry', 'zoereel' ),
		'view_item' => __( 'View Inquiry', 'zoereel' ),
		'search_items' => __( 'Search Project Inquiries', 'zoereel' ),
		'not_found' => __( 'No project inquiries found', 'zoereel' ),
		'not_found_in_trash' => __( 'No project inquiries found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Project Inquiry:', 'zoereel' ),
		'menu_name' => __( 'Project Inquiries', 'zoereel' ),
	);

	$PIargs = array(
		'labels' => $PIlabels,
		'hierarchical' => false,
		'description' => 'Inquiries of projects',
		'supports' => array( 'title','author', 'thumbnail' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);

	register_post_type( 'project_inquiry', $PIargs );

	$JPlabels = array(
		'name' => __( 'Job Postings', 'zoereel' ),
		'singular_name' => __( 'Job Posting', 'zoereel' ),
		'add_new' => __( 'Add Job Posting', 'zoereel' ),
		'add_new_item' => __( 'Add New Job Posting', 'zoereel' ),
		'edit_item' => __( 'Edit Inquiry', 'zoereel' ),
		'new_item' => __( 'New Job Posting', 'zoereel' ),
		'view_item' => __( 'View Posting', 'posting' ),
		'search_items' => __( 'Search Job Postings', 'zoereel' ),
		'not_found' => __( 'No job postings found', 'zoereel' ),
		'not_found_in_trash' => __( 'No job postings found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Job Posting:', 'zoereel' ),
		'menu_name' => __( 'Job Postings', 'zoereel' ),
	);

	$JPargs = array(
		'labels' => $JPlabels,
		'hierarchical' => false,
		'description' => 'Job Postings',
		'supports' => array( 'title','author' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);

	register_post_type( 'posting', $JPargs );

	$SUBlabels = array(
		'name' => __( 'Job Submissions', 'zoereel' ),
		'singular_name' => __( 'Job Submission', 'zoereel' ),
		'add_new' => __( 'Add New', 'zoereel' ),
		'add_new_item' => __( 'Add New Submission', 'zoereel' ),
		'edit_item' => __( 'Edit Submission', 'zoereel' ),
		'new_item' => __( 'New Submission', 'zoereel' ),
		'view_item' => __( 'View Submission', 'zoereel' ),
		'search_items' => __( 'Search Submissions', 'zoereel' ),
		'not_found' => __( 'No submissions found', 'zoereel' ),
		'not_found_in_trash' => __( 'No submissions found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Submission:', 'zoereel' ),
		'menu_name' => __( 'Submissions', 'zoereel' ),
	);
	
	$SUBargs = array(
		'labels' => $SUBlabels,
		'hierarchical' => false,
		'description' => 'Submissions from Job Postings',
		'supports' => array( 'title','editor', 'author' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);
	// Post Type created http://fooplugins.com/generators/wordpress-custom-post-type-code-output/?id=13064
	register_post_type( 'submission', $SUBargs );
	
	$Deallabels = array(
		'name' => __( 'Deals', 'zoereel' ),
		'singular_name' => __( 'Deal', 'zoereel' ),
		'add_new' => __( 'Add New', 'zoereel' ),
		'add_new_item' => __( 'Add New Deal', 'zoereel' ),
		'edit_item' => __( 'Edit Deal', 'zoereel' ),
		'new_item' => __( 'New Deal', 'zoereel' ),
		'view_item' => __( 'View Deal', 'zoereel' ),
		'search_items' => __( 'Search Deals', 'zoereel' ),
		'not_found' => __( 'No deals found', 'zoereel' ),
		'not_found_in_trash' => __( 'No deals found in Trash', 'zoereel' ),
		'parent_item_colon' => __( 'Parent Deal:', 'zoereel' ),
		'menu_name' => __( 'Deals', 'zoereel' ),
	);
	
	$Dealargs = array(
		'labels' => $Deallabels,
		'hierarchical' => false,
		'description' => 'Movie Deals',
		'menu_icon'   => 'dashicons-tickets-alt',
		'supports' => array( 'title', 'author', 'comments' ),
		'taxonomies' => array( 'category', 'post_tag' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_nav_menus' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		// 'rewrite' => false,
		'rewrite' => true,
		// 'capability_type' => 'inquiries'
		'capability_type' => 'post'
	);
	register_post_type( 'deals', $Dealargs );	
}

// ! Register Genre Taxonomy
// created from https://generatewp.com/taxonomy/?clone=music-genre-taxonomy
function genre_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Genres', 'Taxonomy General Name', 'zoereel' ),
		'singular_name'              => _x( 'Genre', 'Taxonomy Singular Name', 'zoereel' ),
		'menu_name'                  => __( 'Genre', 'zoereel' ),
		'all_items'                  => __( 'All Genres', 'zoereel' ),
		'parent_item'                => __( 'Parent Genre', 'zoereel' ),
		'parent_item_colon'          => __( 'Parent Genre:', 'zoereel' ),
		'new_item_name'              => __( 'New Genre Name', 'zoereel' ),
		'add_new_item'               => __( 'Add New Genre', 'zoereel' ),
		'edit_item'                  => __( 'Edit Genre', 'zoereel' ),
		'update_item'                => __( 'Update Genre', 'zoereel' ),
		'view_item'                  => __( 'View Item', 'zoereel' ),
		'separate_items_with_commas' => __( 'Separate genres with commas', 'zoereel' ),
		'add_or_remove_items'        => __( 'Add or remove genres', 'zoereel' ),
		'choose_from_most_used'      => __( 'Choose from the most used genres', 'zoereel' ),
		'popular_items'              => __( 'Popular Items', 'zoereel' ),
		'search_items'               => __( 'Search genres', 'zoereel' ),
		'not_found'                  => __( 'Not Found', 'zoereel' ),
		'no_terms'                   => __( 'No items', 'zoereel' ),
		'items_list'                 => __( 'Items list', 'zoereel' ),
		'items_list_navigation'      => __( 'Items list navigation', 'zoereel' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'genre', array( 'post' ), $args );

}
add_action( 'init', 'genre_taxonomy', 0 );

//Add ACF Front End Content Submission Form
// function my_pre_save_post( $post_id )
// {
// 	wp_mail( $to, $subject, $message, $headers, $attachments ); 
//   
//     // return the new ID
//     return $post_id;
// }
//   
// add_filter('acf/pre_save_post' , 'my_pre_save_post' );

function prefix_register_query_var( $vars ) {
    $vars[] = 'forsale';
    return $vars;
}
add_filter( 'query_vars', 'prefix_register_query_var' );

// https://code.tutsplus.com/articles/custom-page-template-page-based-on-url-rewrite--wp-30564
// TOFIX Improve sale url rewrite for query variable: https://premium.wpmudev.org/blog/building-customized-urls-wordpress/
// https://kinsta.com/blog/wordpress-permalinks-url-rewriting/
function forsale_rewrite_rule() {
	// for_sale ex. https://zoereel.dev/index.php?p=52&for_sale=true
	// for_sale ex. https://zoereel.dev/movie-for-sale/52/?forsale=true
    add_rewrite_rule( 'movie-for-sale/([^/]+)', 'index.php?p=$matches[1]', 'top' );
}
add_action( 'init', 'forsale_rewrite_rule' );


// function append_query_string( $url, $post, $leavename=true ) {
// 	if ( is_page( 360 ) ) {
// 	// if ( $post->post_type == 'post' ) {
// 		$url = $_SERVER['REQUEST_URI'] . $post->post_name; 
// 		$url = add_query_arg( 'sale', 'true', $url );
// 	}
// 	return $url;
// }
// add_filter( 'post_link', 'append_query_string', 10, 3 );


// flush_rewrite_rules();
function create_forsale_url_querystring() {
//     add_rewrite_rule(
//         '^movie-for-sale/([^/]*)$',
//         'index.php?forsale=$matches[1]',
//         'top'
//     );
//     add_rewrite_rule(
//     	'^movie-for-sale/([0-9]+)/([^/]*)/?',
//     	'index.php?p=$matches[1]&forsale=$matches[2]',
//     	'top'
//     );
    add_rewrite_rule(
    	'^movie-for-sale/([0-9]+)/?',
    	'index.php?p=$matches[1]',
    	'top'
    );

    add_rewrite_tag('%forsale%','([^/]*)');
}
// add_action( 'init', 'create_forsale_url_querystring', 10, 0);

// ! Rewrite UM Messages function to get_conversations to remove user check: app/plugins/um-messaging/core/um-messaging-api.php circa line 354
function get_zoereel_conversations( $user_id, $artistpost_id ) {
	global $wpdb;
	$conservation_table = $wpdb->prefix . 'um_conversations';
	// $results = $wpdb->get_results("SELECT * FROM {$conservation_table} WHERE user_a=$user_id OR user_b=$user_id ORDER BY last_updated DESC LIMIT 50");
	if ($user_id == 0) {
		$results = $wpdb->get_results("SELECT * FROM {$conservation_table} WHERE user_a=$artistpost_id OR user_b=$artistpost_id ORDER BY last_updated DESC LIMIT 50");	
	} else {
		$results = $wpdb->get_results("SELECT * FROM {$conservation_table} WHERE user_a=$user_id AND user_b=$artistpost_id OR user_a=$artistpost_id AND user_b=$user_id ORDER BY last_updated DESC LIMIT 50");
	}	
	$wpdb->close();
	$wpdb->db_connect();
// echo '<pre>';
// print_r ($results);
// echo '</pre>';
	if ( $results ) {
		// foreach( $results as $key => $result ) {
		// 	if ( get_userdata( $result->user_b ) === false || get_userdata( $result->user_a ) === false ) {
		// 		unset( $results[$key] );
		// 	}
		// }
		return $results;
	}
	return '';
}

// ! Create New Messages shortcode, function ultimatemember_messages modified from: /app/plugins/um-messaging/core/um-messaging-shortcode.php circa line 68
add_shortcode('um_zoereel_messages', 'um_zoereel_messages');

function um_zoereel_messages( $args = array() ) {
	global $ultimatemember, $um_messaging;

	$defaults = array(
		'user_id' => get_current_user_id(),
		'artistpost_id' => 0
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args );

	ob_start();
	
	$conversations = get_zoereel_conversations( $user_id, $artistpost_id );

	if ( isset( $_GET['conversation_id'] ) ) {
		if ( esc_attr( absint( $_GET['conversation_id'] ) ) ) {
			foreach( $conversations as $conversation ) {
				if ( $conversation->conversation_id == $_GET['conversation_id'] )
					// $current_conversation = esc_attr( absint( $_GET['conversation_id'] ) );
					$current_conversation = $conversation->conversation_id;
					continue;
			}
		}
	}

	if ( file_exists( get_stylesheet_directory() . '/ultimate-member/templates/conversations.php' ) ) {
		include_once get_stylesheet_directory() . '/ultimate-member/templates/conversations.php';
	} else {
		include_once um_messaging_path . 'templates/conversations.php';
	}
	
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}

// ! Rewrite UM Messages function to get_conversation: app/plugins/um-messaging/core/um-messaging-api.php circa line 410
	function get_zoereel_conversation( $user1, $user2, $conversation_id = null ) {
		global $wpdb;
		$message_table = $wpdb->prefix . 'um_messages';
		// No conversation yet
		if ( !$conversation_id || $conversation_id <= 0 ) return;

		// Get conversation ordered by time and show only 1000 messages
		$messages = $wpdb->get_results("SELECT * FROM {$message_table} WHERE conversation_id=$conversation_id ORDER BY time ASC LIMIT 1000");
		$wpdb->close();
		$wpdb->db_connect();

		$response = null;
		$update_query = false;
		foreach( $messages as $message ) {

			if ( $message->status == 0 ) {
				$update_query = true;
				$status = 'unread';
			} else {
				$status = 'read';
			}

			if ( $message->author == get_current_user_id() ) {
				$class = 'right_m';
				$remove_msg = '<a href="#" class="um-message-item-remove um-message-item-show-on-hover um-tip-s" title="'. __('Remove','um-messaging').'"></a>';
			} else {
				$class = 'left_m';
				$remove_msg = '';
			}

			$response .= '<div class="um-message-item ' . $class . ' ' . $status . '" data-message_id="'.$message->message_id.'" data-conversation_id="'.$message->conversation_id.'">';

			// $response .= '<div class="um-message-item-content">' . $this->chatize( $message->content ) . '</div><div class="um-clear"></div>';
			$response .= '<div class="um-message-item-content">' . $message->content . '</div><div class="um-clear"></div>';

			// $response .= '<div class="um-message-item-metadata">' . $this->beautiful_time( $message->time, $class ) . '</div><div class="um-clear"></div>';
			$response .= '<div class="um-message-item-metadata">' . beautiful_time($message->time, $class) . '</div><div class="um-clear"></div>';

			$response .= $remove_msg;

			$response .= '</div><div class="um-clear"></div>';

		}

		if ( $update_query ) {
			$logged = get_current_user_id();
			$wpdb->query("UPDATE {$message_table} SET status=1 WHERE conversation_id={$conversation_id} AND author != {$logged}");
			$wpdb->close();
			$wpdb->db_connect();


		}


		return $response;
	}

// ! Rewrite UM Messages function to conversation_template: app/plugins/um-messaging/core/um-messaging-api.php circa line 276
	function conversation_zoereel_template( $message_to, $user_id, $artistpost_id, $isartist ) {
		global $ultimatemember;
		um_fetch_user( $message_to );
echo '<br>$message_to: ' . $message_to . '  $user_id: ' . $user_id . '  $artistpost_id: ' . $artistpost_id . '  $isartist: ' . $isartist;
		$contact_name = ( um_user('display_name') ) ? um_user('display_name') : __('Deleted User','um-messaging');
		$contact_url = um_user_profile_url();

		$limit = um_get_option('pm_char_limit');

		um_fetch_user( $user_id );

		// $response = $this->get_conversation_id( $message_to, $user_id );
		if ($isartist == 0 ) {
			$response = get_zoereel_conversations( $message_to, $artistpost_id );
			$data_message_to = $message_to;
		} else {
			$response = get_zoereel_conversations( $artistpost_id, $user_id );
			$data_message_to = $artistpost_id;	
		}
		// $response = get_zoereel_conversations( $artistpost_id, $user_id );		
		$message_history = add_query_arg('profiletab', 'messages', um_user_profile_url() );

		?>

			<div class="um-message-header um-popup-header">
				<div class="um-message-header-left seeequme"><?php echo get_avatar( $message_to, 40 ); ?> <?php echo '<a href="'. um_user_profile_url() . '">' . $contact_name . '</a>'; ?></div>
				<div class="um-message-header-right">
					<a href="#" class="um-message-blocku um-tip-e" title="<?php _e('Block user','um-messaging'); ?>" data-other_user="<?php echo $message_to; ?>" data-conversation_id="<?php echo $response[0]->conversation_id; ?>"><i class="um-faicon-ban"></i></a>
					<a href="#" class="um-message-delconv um-tip-e" title="<?php _e('Delete conversation','um-messaging'); ?>" data-other_user="<?php echo $message_to; ?>" data-conversation_id="<?php echo $response[0]->conversation_id; ?>"><i class="um-icon-trash-b"></i></a>
					<a href="#" class="um-message-hide um-tip-e" title="<?php _e('Close chat','um-messaging'); ?>"><i class="um-icon-android-close"></i></a>
				</div>
			</div>

			<div class="um-message-body um-popup-autogrow um-message-autoheight" data-message_to="<?php echo $data_message_to; // echo $message_to; ?>">
				<div class="um-message-ajax" data-message_to="<?php echo $data_message_to; // echo $message_to; ?>" data-conversation_id="<?php echo $response[0]->conversation_id; ?>" data-last_updated="<?php echo $response[0]->last_updated; ?>">

					<?php 
						echo get_zoereel_conversation( $message_to, $user_id, 2 );
					// if ( $this->perms['can_read_pm'] ) {
					// 		echo $this->get_conversation( $message_to, $user_id, $response[0]->conversation_id );
					// } else {
					// 
					// 	echo '<span class="um-message-notice">' . __('Your membership level does not allow you to view conversations.','um-messaging') . '</span>';
					// 
					// }

					?>

				</div>
			</div>

			<div class="um-message-footer um-popup-footer" data-limit_hit="<?php _e('You have reached your limit for sending messages.','um-messaging'); ?>" >
				<?php // if ( $this->limit_reached() ) { ?>
				<?php // _e('You have reached your limit for sending messages.','um-messaging'); ?>
				<?php // } else if ( $this->perms['can_reply_pm'] ) { ?>
				<div class="um-message-textarea">
					<?php // echo $this->emoji(); ?>
					<textarea name="um_message_text" id="um_message_text" data-maxchar="<?php echo $limit; ?>" placeholder="<?php _e('Type your message...','um-messaging'); ?>"></textarea>
				</div>
				<div class="um-message-buttons">
					<span class="um-message-limit"><?php // echo $limit; ?></span>
					<a href="#" class="um-message-send disabled"><i class="um-faicon-envelope-o"></i><?php _e('Send message','um-messaging'); ?></a>
				</div>
				<div class="um-clear"></div>
				<?php // } else { ?>
				<?php // _e('You are not allowed to reply to private messages.','um-messaging'); ?>
				<?php // } ?>
			</div>
	<?php
	}

	// ! transfer UM Messages function to human_time_diff: app/plugins/um-messaging/core/um-messaging-api.php circa line 481
	function human_time_diff_zoereel( $from, $to = '' ) {
		if ( empty( $to ) ) {
			$to = time();
		}

		$diff = (int) abs( $to - $from );


		if ( $diff < 60 ) {
			$since = sprintf( __('%ss','um-messaging'), $diff );
		} elseif ( $diff < HOUR_IN_SECONDS ) {
			$mins = round( $diff / MINUTE_IN_SECONDS );
			if ( $mins <= 1 )
				$mins = 1;
			/* translators: min=minute */
			$since = sprintf( __('%sm','um-messaging'), $mins );
		} elseif ( $diff < DAY_IN_SECONDS && $diff >= HOUR_IN_SECONDS ) {
			$hours = round( $diff / HOUR_IN_SECONDS );
			if ( $hours <= 1 )
				$hours = 1;
			$since = sprintf( __('%sh','um-messaging'), $hours );
		} elseif ( $diff < WEEK_IN_SECONDS && $diff >= DAY_IN_SECONDS ) {
			$days = round( $diff / DAY_IN_SECONDS );
			if ( $days <= 1 )
				$days = 1;
			$since = sprintf( __('%sd','um-messaging'), $days );
		} elseif ( $diff < 30 * DAY_IN_SECONDS && $diff >= WEEK_IN_SECONDS ) {
			$weeks = round( $diff / WEEK_IN_SECONDS );
			if ( $weeks <= 1 )
				$weeks = 1;
			$since = sprintf( __('%sw','um-messaging'), $weeks );
		} elseif ( $diff < YEAR_IN_SECONDS && $diff >= 30 * DAY_IN_SECONDS ) {
			$months = round( $diff / ( 30 * DAY_IN_SECONDS ) );
			if ( $months <= 1 )
				$months = 1;
			$since = sprintf( __('%sm','um-messaging'), $months );
		} elseif ( $diff >= YEAR_IN_SECONDS ) {
			$years = round( $diff / YEAR_IN_SECONDS );
			if ( $years <= 1 )
				$years = 1;
			$since = sprintf( __('%sy','um-messaging'), $years );
		}

		return apply_filters( 'um_messaging_human_time_diff', $since, $diff, $from, $to );
	}
	
	// ! transfer UM Messages function to beautiful_time: app/plugins/um-messaging/core/um-messaging-api.php circa line 533
	function beautiful_time( $time, $pos ) {
		$from_time_unix = strtotime( $time );
		$offset = get_option( 'gmt_offset' );
		$offset = apply_filters("um_messages_time_offset", $offset );

		$from_time = $from_time_unix - $offset * HOUR_IN_SECONDS; 
		$from_time = apply_filters("um_messages_time_from", $from_time, $time );

		$current_time = current_time('timestamp') - $offset * HOUR_IN_SECONDS;
		$current_time = apply_filters("um_messages_current_time", $current_time );

		$nice_time = human_time_diff_zoereel( $from_time, $current_time  );
		$nice_time = apply_filters("um_messages_time_nice", $nice_time, $from_time, $current_time );

		$clean_date_time = date("F d, Y, h:i A", $from_time );
		$clean_date_time = apply_filters("um_messages_time_clean", $clean_date_time, $from_time );
         
        $pos = apply_filters("um_messages_time_position", $pos );
         
		if ( $pos == 'right_m' ) {
			return '<span class="um-message-item-time um-tip-e" title="" um-messsage-timestamp="'.$from_time.'" um-message-utc-time="'.$clean_date_time.'">' . $nice_time . '</span>';
		} else {
			return '<span class="um-message-item-time um-tip-w"  title="" um-messsage-timestamp="'.$from_time.'" um-message-utc-time="'.$clean_date_time.'">' . $nice_time . '</span>'; 
		}
	}

// function prefix_url_rewrite_templates() {
// 
//     if ( get_query_var( 'forsale' ) && is_singular( 'forsale' ) ) {
//         add_filter( 'template_include', function() {
//             return get_template_directory() . '/taxonomy-for_sale.php';
//         });
//     }
// }
// add_action( 'template_redirect', 'prefix_url_rewrite_templates' );

// // make Videos hierarchical for episodic videos
// add_action('registered_post_type', 'igy2411_make_posts_hierarchical', 10, 2 );
// 
// // Runs after each post type is registered
// function igy2411_make_posts_hierarchical($post_type, $pto){
//     // Return, if not post type posts
//     if ($post_type != 'post') return;
//     // access $wp_post_types global variable
//     global $wp_post_types;
//     // Set post type "post" to be hierarchical
//     $wp_post_types['post']->hierarchical = 1;
//     // Add page attributes to post backend
//     // This adds the box to set up parent and menu order on edit posts.
//     add_post_type_support( 'post', 'page-attributes' );
// 
// }
// add_filter( 'page_attributes_dropdown_pages_args', 'mwm_show_only_author_pages_in_attributes' );
// add_filter( 'quick_edit_dropdown_pages_args', 'mwm_show_only_author_pages_in_attributes' );
// 
// function mwm_show_only_author_pages_in_attributes( $args ) {
//     global $post;
//     $args['author'] = $post -> post_author;
//     return $args;
// }

/**
// ! Custom Happy Hippopotamus Not In Use
**
**
*************/

/** https://www.templatemonster.com/blog/add-custom-user-meta-data-wordpress/
 * Add new fields above 'Update' button.
 *
 * @param WP_User $user User object.
 */
function tm_additional_profile_fields( $user ) {

    $months 	= array( 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' );
    $default	= array( 'day' => 26, 'month' => 'October', 'year' => 1985, );
    $birth_date = wp_parse_args( get_the_author_meta( 'birth_date', $user->ID ), $default );

    ?>

    <table class="form-table">
	 <tr>
	 	<th><label for="twitter">Twitter</label></th>
	 
	 	<td>
	 		<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
	 		<span class="description">Twitter username.</span>
	 	</td>
	 </tr>
   	 <tr>
   		 <th><label for="birth-date-day">Birth date</label></th>
   		 <td>
   			 <select id="birth-date-day" name="birth_date[day]"><?php
   				 for ( $i = 1; $i <= 31; $i++ ) {
   					 printf( '<option value="%1$s" %2$s>%1$s</option>', $i, selected( $birth_date['day'], $i, false ) );
   				 }
   			 ?></select>
   			 <select id="birth-date-month" name="birth_date[month]"><?php
   				 foreach ( $months as $month ) {
   					 printf( '<option value="%1$s" %2$s>%1$s</option>', $month, selected( $birth_date['month'], $month, false ) );
   				 }
   			 ?></select>
   			 <select id="birth-date-year" name="birth_date[year]"><?php
   				 for ( $i = 1950; $i <= 2015; $i++ ) {
   					 printf( '<option value="%1$s" %2$s>%1$s</option>', $i, selected( $birth_date['year'], $i, false ) );
   				 }
   			 ?></select>
   		 </td>
   	 </tr>
    </table>  
    <?php
}

// add_action( 'show_user_profile', 'tm_additional_profile_fields' );
// add_action( 'edit_user_profile', 'tm_additional_profile_fields' );

/**
 * Save additional profile fields.
 *
 * @param  int $user_id Current user ID.
 */
function tm_save_profile_fields( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
   	 return false;
    }
	update_usermeta( $user_id, 'twitter', $_POST['twitter'] );
	
    if ( empty( $_POST['birth_date'] ) ) {
   	 return false;
    }

    update_usermeta( $user_id, 'birth_date', $_POST['birth_date'] );
}

// add_action( 'personal_options_update', 'tm_save_profile_fields' );
// add_action( 'edit_user_profile_update', 'tm_save_profile_fields' );