<!-- templates/content-large -->
<?php
$file = get_field('video');
if( $file ): 
	
	$image   = wp_get_attachment_image_src( get_field('cover_art'), 'poster-big' );
	$title   = wp_trim_words( get_the_title(), $num_words = 10, $more = '... ' );
	// $content = wp_trim_words( strip_tags(get_field('video_description')), 15, ' <a class="show-more-content" data-id="' . get_the_ID() . '">' . __( 'read more', 'zoereel' ) . '</a>' );
	$content = wp_trim_words( strip_tags(get_field('video_description')), 15, __( 'read more', 'zoereel' ) );
?>

<div class="featureContainer touch" style="background-image: url(<?php echo esc_url($image[0]); ?>);">

	<article class="content-overlay">
		<div class="content-overlay-grad hidden-xs-down"></div>
        <a class="overlay-link" href="<?php the_permalink(); ?>">
        <div class="container-fluid rel">
        	<div class="rowOFF rel">
        		<div class="col-9 rel">
        			<div class="synopis-outer">
        				<div class="synopis-middle">
        					<div class="synopis-inner">
        						<h2><?php echo (isset($title) ? $title : __( 'No Title', 'zoereel' )); ?></h2>
        						<div class="synopis content">
        							<?php echo $content; ?>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div></a>
		<?php get_template_part('partials/video', 'metrics'); ?>
	</article>
	<!--/.content-overlay-->

</div>

<?php endif; ?>