<header id="header" class="banner">
  <div class="container">
	  
<!--    https://codepen.io/erikterwan/pen/EVzeRP    -->
<nav id="mobileNav" role="navigation">
  <div id="menuToggle">
    <!-- hidden checkbox used as click reciever -->
    <input type="checkbox" />
    
    <!-- hamburger spans -->
    <div class="burger" data-toggle="modalOFF" data-target="#pulloutOFF">
	    <span></span>
	    <span></span>
	    <span></span>
    </div>
    
    <section id="pullout" class="modalOFF fadeOFF">
	    <div id="mobileMenuWrapper">
    
	  <?php 
 		  um_notification_show_feed(); 
 		  
          if (has_nav_menu('mobile-menu')) :
            wp_nav_menu(['theme_location' => 'mobile-menu', 'menu_id' => 'mobileAccount', 'container' => '']);
          endif;
 
          if (has_nav_menu('browsem')) :
            wp_nav_menu(['theme_location' => 'browsem', 'menu_id' => 'mobileBrowse', 'container' => '']);
          endif;
	  ?> 	     
	    </div>
    </section>  
  </div>
  <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="/wp-content/themes/zoereel/dist/images/color-white-text.svg" alt="Zoereel" /><span>Beta</span></a>
  <a class="upload" title="Upload a New Video Video" href="https://zoereel.com/new-upload/"><span itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="um-faicon-cloud-upload menu-item"></span></a>
  <div class="search mobile">
<!--   	  <span id="trigger-search" class="search_button burger" type="button" data-target="headerSearch"> <i class="fa fa-search"></i> </span> -->
  	  <?php get_search_form(); ?>
  </div>
</nav>
	  	
  </div>
</header>
