<?php // get_template_part('templates/page', 'header'); ?>

<?php $category = get_queried_object(); ?>

<article class="scroll__rows">
	
<?php if ($category->term_id == 33 || $category->term_id == 1 ) { // 33 - TV Shows, 1 - Short Films  ?>

	<?php 
	$postsperrow = 20; $genreIDs = get_field('category_genre', 'option');
	
	if( $genreIDs ): 
	
		foreach ($genreIDs as &$genreID) {
			
			$genre = get_term( $genreID, 'genre' );
	?>
		<h4><?php echo $category->name . ' - ' . $genre->name ?></h4>
		<section class="row">
			<span class="handle handlePrev" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-left"></span></span>
			<div class="row__inner">
	<?php
			$args= array(
				'post_type' => 'post',
				'numberposts'	=> $postsperrow,
				'cat'       => $category->term_id,
				'tax_query' => array(
					array(
						'taxonomy' => 'genre',
						'field'    => 'ID',
						'terms'    => $genreID,
					),
				)
			);
			$videos = new WP_Query($args);
			while($videos->have_posts()) : $videos->the_post();
				get_template_part('templates'.$isMobile.'/video-tile', get_post_type() != 'post' ? get_post_type() : get_post_format());
			endwhile;
			wp_reset_postdata();
	?>
		    </div>
		    <span class="handle handleNext show" tabindex="0" role="button" aria-label="More Videos"><span class="fa-angle-right"></span></span>
		</section>
	<?php
		}
	
	endif;
	
	?>

<?php } else { ?>

<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>

	<?php if (!have_posts()) : ?>
	  <div class="alert alert-warning">
	    <?php echo $category->name. ' '; _e('Coming Soon!', 'sage'); ?>
	  </div>
	  <?php get_search_form(); ?>
	<?php endif; ?>
	
	<section class="row video-grid">
		<div class="row__inner">
			<?php // Loop
			while (have_posts()) : the_post();
				if( get_field('cover_art') && get_field('for_sale') != true ):
					// get_template_part('templates'.$isMobile.'/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
					get_template_part('templates/video', 'grid-tile');
				endif;
			endwhile;
			wp_reset_postdata();
			?>
	    </div>
	</section>

<?php } ?>

</article>

<?php // the_posts_navigation(); ?>
