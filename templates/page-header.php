<?php use Roots\Sage\Titles; ?>

<div class="page-header">
	<?php if (!is_home() && !is_ultimatemember() && !is_page() ) { ?>
		<h1><?= Titles\title(); ?></h1>
  	<?php } ?>
</div>
