<!-- templates/comments-rt -->
<?php
if (post_password_required()) {
  return;
}
?>
<?php
 	$reeltalkNum = get_comments(
 		array(
 		'status' => 'approve',
 		'post_id'=> $post->ID,
 		'type'=> 'reeltalk',
 		'count' => true)
 	);
?>
<section id="reeltalk" class="comments">
  <?php if (have_comments()) : ?>
  	<h4>ReelTalk is only for Filmmakers and Studios</h4>
    <h2><?php printf(_nx('One ReelTalk to &ldquo;%2$s&rdquo;', '%1$s conversations about &ldquo;%2$s&rdquo;', $reeltalkNum, 'comments title', 'sage'), number_format_i18n($reeltalkNum), '<span>' . get_the_title() . '</span>'); ?></h2>

<!--     <button id="toggle-all-rt" class="btn btn-outline-secondary">Toggle all threaded reeltalks</button> -->
    <ol id="reeltalk-list" class="comment-list">
      <?php
		  // https://codex.wordpress.org/Function_Reference/wp_list_comments
		  wp_list_comments(['type' => 'reeltalk', 'style' => 'ol', 'short_ping' => true, 'callback' => 'format_comment', 'comment_type' => 'reeltalk']);
	  ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; // have_comments() ?>

  <?php // https://codex.wordpress.org/Function_Reference/comment_form
$argsrt = array(
  'id_form'           => 'reeltalkform',
  'class_form'      => 'comment-form',
  'id_submit'         => 'submit',
  'class_submit'      => 'submit',
  'name_submit'       => 'submit',
  'title_reply'       => __( 'Filmmakers Private Conversation' ),
  'title_reply_before' => __( '<h5 id="reply-title" class="comment-reply-title">' ),
  'title_reply_after' => __( '</h5>' ),
  'title_reply_to'    => __( 'Reply to %s' ),
  'cancel_reply_link' => __( 'Cancel Reply' ),
  'label_submit'      => __( 'Post Your ReelTalk' ),
  'format'            => 'xhtml',

  'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( '', 'noun' ) .
    '</label><textarea id="reeltalkcomment" name="comment" cols="45" rows="3" aria-required="true">' .
    '</textarea></p>',

  'must_log_in' => '<p class="must-log-in">' .
    sprintf(
      __( 'You must be <a href="%s">logged in</a> to discuss.' ),
      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
    ) . '</p>',

    'logged_in_as' => '',

  'comment_notes_before' => '<p class="comment-notes">' .
    __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) .
    '</p>',

  // 'fields' => apply_filters( 'comment_form_default_fields', $fields ),
);
	echo '<div id="reeltalkFormWrap">';
	  comment_form($argsrt);
	echo '</div>';
  ?>
</section>

<script>
// $(document).ready(function() {
// 
//     // Toggle all
//     $('#toggle-all-rt').click(function() {
//         var $subcomments = $('#reeltalk-list').find('> li > ul');
//         if ($subcomments.filter(':hidden').length) {
//             $subcomments.slideDown('fast', function() {
// 	            $(this).prev('.toggle').text('Hide All Reeltalk Replys')
//             });
//         } else {
//             $subcomments.slideUp('fast', function() {
// 	            $(this).prev('.toggle').text('View All Reeltalk Replys')
//             });
//         }
//     });
// 
//     // Add buttons to threaded comments
//     $('#reeltalk-list').find('> li > ul')
//         .before('<span class="toggle">View All Reeltalk Replys</span>'); // toggle with "Hide all Replys"
// 
//    // Toggle one section
//     $('#reeltalk-list').find('span.toggle').click(function() {
//          $(this).next('ul').slideToggle('fast', function() {
//  		 	if ($(this).is(':visible')) { $(this).prev('.toggle').text('Hide All Reeltalk Replys') }
//  		 	else { $(this).prev('.toggle').text('View All Reeltalk Replys') }
//  		 });
//     });
// });
</script>