<?php
$vidID = get_the_ID();
$surveys = get_posts(array(
		'post_type' => array( 'survey' ),
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'video_id',
				'value' => $vidID,
				'compare' => 'LIKE'
			)
		)
	));
?>

<br>
<br>
<article <?php echo post_class() ?>>
  <header>
    <h2>(<?php echo count($surveys); ?>) Surveys for <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
  </header>
  <br>
  <br>
  <div class="entry-summary">
	  <table id="survey-table">
<?php
if ( $surveys )
{
	foreach ( $surveys as $post )
	{
		setup_postdata( $post ); ?>
	    		<tr>
	    			<td><a href="<?= um_user_profile_url(); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></td>  <td><span class="survey-link" data-surveyid="<?= get_the_ID(); ?>" data-url="<?php the_permalink(); ?>" data-toggle="modal" data-target="#surveyModal">View Survey</span></td>
	    		</tr>
	    		<?php
	}
}
wp_reset_postdata();
?>
	  </table>
	  <br>
	  <br>
	  <a class="btn btn-secondary" href="<?php // echo get_bloginfo('url'); ?>/analytics/?most-surveyed">See Most Surveyed</a>
  </div>
</article>
<?php
$subject = 'Check this survey out on ZOEREEL';
$emailaddress = 's.spielberg@studio.co';
$body = 'Here’s a Survey done on ' . get_the_title() . '%0D%0ABest,%0D%0AZOEREEL TEAM';
?>
<!-- Modal -->
<div id="surveyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">X</button>
        <h4 class="modal-title"><a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a></h4>
      </div>
      <div id="surveyBody" class="modal-body">
        <p>Survey content goes here</p>
      </div>
      <div class="modal-footer">
			<a class="" href="mailto:<?php echo urlencode($emailaddress); ?>?subject=<?php echo urlencode(html_entity_decode($subject, ENT_COMPAT, 'UTF-8')); ?>&body=<?php echo $body ?>" title="<?php _e('Email this Survey', 'grotto'); ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>

        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
jQuery(document).ready(function () {
    jQuery('.survey-link').click(function() {
	    jQuery(this).addClass('read');
        var url = jQuery(this).data('url'), $surveyid = jQuery(this).data('surveyid'),
        	viewnonce = '<?php echo wp_create_nonce( 'pvc-check-post' ) ?>';

        jQuery('#surveyBody').load( url + " #surveyContent", function() {
			console.log( "The survey been loaded for: " + $surveyid );
			<?php // NOTE: taken from: plugins/post-views-counter/includes/frontend.php circa line 178 ?>
			var request = {
				action: 'pvc-check-post',
				pvc_nonce: viewnonce,
				id: $surveyid
			};

			jQuery.ajax( {
				url: 'https:\/\/zoereel.com\/wp\/wp-admin\/admin-ajax.php',
				type: 'post',
				async: true,
				cache: false,
				data: request
			} );
		});
    });
});
</script>
