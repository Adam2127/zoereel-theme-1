<section id="entryContent"><?php the_content(); ?></section>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
<?php the_field('inline_js'); ?>