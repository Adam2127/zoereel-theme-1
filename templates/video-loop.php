<!-- templates/video-loop -->
<?php $queryVar = (isset($queryVar)) ? $queryVar : ''; ?>
<div class="um um-item">
    <div class="zr-item-poster">
        <a href="<?php the_permalink(); echo $queryVar; ?>"><?php echo wp_get_attachment_image( get_field('cover_art'), 'poster-sm', false, 'class=alignleft' ); ?></a>
        <a class="play-icon-wrap" href="<?php the_permalink(); echo $queryVar; ?>">
        <div class="content-overlay">
            <span class="play-icon-wrap">
            <div class="play-icon-wrap-relOFF">
                <div class="play-icon-wrap-rel-ring"></div><span class="play-icon-wrap-rel-play"><i aria-hidden="true" class="fa fa-play fa-2x"></i></span>
            </div>
            </span>
        </div>
        </a>
    </div>
    <div class="zr-item-meta">
        <div class="um-item-link">
            <a href="<?php the_permalink(); echo $queryVar ?>"><h3><?php the_title(); ?></h3></a>
        </div>
        <div class="zr-item-desc">            
			<?php
			if (isset($getcomments)) {
				$lastcommentID = 0;	
				$reeltalk = ($commenttype == 'reeltalk') ? true : false;	
				$args = array(
						'post_id' => $post->ID,
						'type' => $commenttype,
					);
				$vidcomments = get_comments($args); $i=0;			
				echo '<span>' . count($vidcomments) . ' ' . (($commenttype == "reeltalk") ? "ReelTalks" : "Comments" ) . '</span>';
				
				foreach($vidcomments as $vidcomment) :
					if($i==3) break; $i++; 
						// include( locate_template( 'partials/comment-text.php', false, false ) );
						// get_template_part('partials/comment', 'text');
						$lastcommentID = $vidcomment->comment_ID;
					?>
					<?php $commentlink = ($reeltalk == true) ? str_replace('comment','reeltalk',get_comment_link($vidcomment->comment_ID)) : get_comment_link( $vidcomment->comment_ID );  ?>
					<div class="um-item-link w-icon"><a href="<?php echo $commentlink; ?>"><i class="um-icon-chatboxes"></i></a><?php echo get_comment_excerpt( $vidcomment->comment_ID ); ?></div>
					<?php 
				endforeach;
				
				$commentlink = ($reeltalk == true) ? str_replace('comment','reeltalk',get_comment_link( $lastcommentID )) : get_comment_link( $lastcommentID );
				
				echo '<div class="viewall"><a href="'. $commentlink . '" class="btn btn-secondary btn-sm"><b>' . (($reeltalk == true) ? __("View All ReelTalks","zoereel") : __("View All Comments","zoereel")) . '</b></a></div>';
			} else { ?>
				<span class="published"><?php echo sprintf(__('%s ago','ultimate-member'), human_time_diff( get_the_time('U'), current_time('timestamp') ) ); ?></span>
				<?php 
					if (isset($getsurveys)) {
						$surveyNum = get_post_field( 'surveys_count', $post->ID );
						echo '| <span>' . $surveyNum . ' surveys</span>';
					}
				?>				
	            <?php // echo '<p>' . wp_trim_words( get_field('video_description' ), $num_words = 18, $more = '...' ) . '</p>'; ?>
				<?php
				$long_text = get_field('video_description' );
				
				$max_length = wp_is_mobile() ? 60 : 97; // we want to show only 40 characters.
				
				if (strlen($long_text) > $max_length)
				{
					$short_text = (substr($long_text,0,$max_length-1)); // make it $max_length chars long
					$short_text .= "..."; // add an ellipses ... at the end
					// $short_text .= "<a href='#'>Read more</a>"; // add a link
					echo '<p>' . $short_text . '</p>';
				} else {
					// string is already less than $max_length, so display the string as is
					echo '<p>' . $long_text . '</p>';
				}					
				if (isset($showmetrics)) {	
					get_template_part('partials/video', 'metrics');	
				}	
			}
					if (isset($getsurveys)) {
						echo '<br><div style="text-align:left;"><a href="' . get_the_permalink() . '?surveys" class="btn btn-secondary">View All</a></div>';
					}			
			?>             
		</div> 
    </div>
</div>
