<!-- templates/comments -->
<?php
if (post_password_required()) {
  return;
}
?>
<?php
$commentNum = get_comments(
    array(
    'status' => 'approve',
    'post_id'=> $post->ID,
    'type'=> 'comment',
    'count' => true)
 );
?>
<section id="comment" class="comments">
  <?php if (have_comments()) : ?>
    <h4><?php printf(_nx('One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', $commentNum, 'comments title', 'sage'), number_format_i18n($commentNum), '<span>' . get_the_title() . '</span>'); ?></h4>
<!-- 	<button id="toggle-all" class="btn btn-outline-secondary">Toggle all threaded comments</button> -->
    <ol id="comment-list" class="comment-list">
      <?php // wp_list_comments(['type' => 'comment', 'style' => 'ol', 'short_ping' => true, 'reverse_top_level' => true, 'callback' => 'zoereel_comment']); ?>
      <?php wp_list_comments('type=comment&callback=format_comment&reverse_top_level=true&comment_type=comment'); ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; // have_comments() ?>

  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'sage'); ?>
    </div>
  <?php endif; ?>

  <?php
// https://codex.wordpress.org/Function_Reference/comment_form
$args = array(
  'id_form'           => 'commentform',
  'class_form'      => 'comment-form',
  'id_submit'         => 'submit',
  'class_submit'      => 'submit',
  'name_submit'       => 'submit',
  'title_reply'       => __( 'Discuss this Video' ),
  'title_reply_before' => __( '<h5 id="reply-title" class="comment-reply-title">' ),
  'title_reply_after' => __( '</h5>' ),
  'title_reply_to'    => __( 'Reply to %s' ),
  'cancel_reply_link' => __( 'Cancel Reply' ),
  'label_submit'      => __( 'Post Comment' ),
  'format'            => 'xhtml',
  'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
    '</label><textarea id="comment" name="comment" cols="45" rows="3" aria-required="true">' .
    '</textarea></p>',
  'must_log_in' => '<p class="must-log-in">' .
    sprintf(
      __( 'You must be <a href="%s">logged in</a> to discuss.' ),
      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
    ) . '</p>',
  'logged_in_as' => '',
  'comment_notes_before' => '<p class="comment-notes">' .
    __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) .
    '</p>',

);
	// echo '<div id="commentFormWrap">';
	comment_form($args);
	// echo '</div>';
	// str_replace("respond","commentFormWrap",comment_form($args));
  ?>
</section>

<script>
// document.getElementById('respond').id = 'respondreeltalk';

$(document).ready(function() {

    // Toggle all
    // $('#toggle-all').click(function() {
    //     var $subcomments = $('#comment-list').find('> li > ul');
    //     if ($subcomments.filter(':hidden').length) {
    //         $subcomments.slideDown('fast', function() {
	//             $(this).prev('.toggle').text('Hide All Replys')
    //         });
    //     } else {
    //         $subcomments.slideUp('fast', function() {
	//             $(this).prev('.toggle').text('View All Replys')
    //         });
    //     }
    // });

    // Add buttons to threaded comments
    $('#comment-list').find('> li > ul')
        .before('<span class="toggle">View All Replys</span>'); // toggle with "Hide all Replys"

   // Toggle one section
    $('#comment-list').find('span.toggle').click(function() {
         $(this).next('ul').slideToggle('fast', function() {
 		 	if ($(this).is(':visible')) { $(this).prev('.toggle').text('Hide All Replys') }
 		 	else { $(this).prev('.toggle').text('View All Replys') }
 		 });
    });
});
</script>
