<article <?php post_class('tile'); ?>>
<?php
echo '<a href="' . get_permalink() . '"><div class="tile__media">' . get_the_post_thumbnail($post->ID, 'poster-sm', array( 'class' => 'alignleft' ) ) .'</div><div class="tile__details"></div></a>';
// get_template_part('partials/video', 'metrics');
include( locate_template( 'partials/video-metrics.php', false, false ) ); 
?>
</article>