<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header>
  <div class="entry-summary">
	<div class="flex">  
  	<div class="col-sm-6">
	  <dl>
		  <dt>Production Type</dt>
		  <dd><?php $term = get_term( get_field('production_type'), 'production_type' ); echo $term->name; ?></dd>
		  <dt>Production Company</dt>
		  <dd><?php the_field('production_company'); ?></dd>
		  <dt>Director</dt>
		  <dd><?php the_field('production_director'); ?></dd>
		  <dt>Casting Director</dt>
		  <dd><?php the_field('casting_director'); ?></dd>		  
		  <dt>Production Description</dt>
		  <dd><?php the_field('production_description'); ?></dd>	 
	  </dl>
  	</div>
  	<div class="col-sm-6">  	  
	  <dl>		  
		  <?php if( have_rows('production_jobs') ): ?>
		  <dt>Job Postions</dt>
		    <?php while( have_rows('production_jobs') ): the_row(); ?>
		        <dd><?php the_sub_field('job_postion'); ?><?php echo ( get_sub_field('application_requirements')  ? ' - <small>video reel required</small>' : '' ); ?></dd>	        
		    <?php endwhile; ?>	 
		   <?php endif; ?> 
		  <dt>Production Locations & Dates</dt>
		  <dd><?php the_field('production_dates_locations'); ?></dd>
		  <dt>Union Status</dt>
		  <dd><?php the_field('union_staus'); ?></dd>
		  <dt>Payment Info</dt>
		  <dd><?php the_field('production_payment_info'); ?></dd>			   
	  </dl>
  	</div>  
	</div>
  </div>
</article>
