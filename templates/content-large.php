<!-- templates/content-large -->
<?php
	$file = get_field('video');
	if( $file ): ?>

<!--
<article class="tile">
	<a href="<?php // the_permalink(); ?>" title="<?php // the_title_attribute(); ?>">
	<div class="tile__media">

			<?php // the_post_thumbnail('poster-big', ['class' => 'featured', 'title' => 'Feature Video']); ?>
	</div>
	<div class="tile__details">
		<div class="tile__title">
			<?php // the_title(); ?>
  		</div>
	</div>	
	</a>
</article>
-->

<?php
// taken from /themes/netflix/templates/content-slider.php circa line 19
	$image   = wp_get_attachment_image_src( get_field('cover_art'), 'poster-big' ); 
	$title   = wp_trim_words( get_the_title(), $num_words = 10, $more = '... ' );
	// $streamiumVideoTrailer = get_post_meta( get_the_ID(), 'streamium_video_trailer_meta_box_text', true );
	// $streamiumFeaturedVideo = get_post_meta( get_the_ID(), 'streamium_featured_video_meta_box_text', true );
	// $nonce = wp_create_nonce( 'streamium_likes_nonce' );
	// $link = admin_url('admin-ajax.php?action=streamium_likes&post_id='.get_the_ID().'&nonce='.$nonce);
	$content = wp_trim_words( strip_tags(get_field('video_description')), 15, ' <a class="show-more-content" data-id="' . get_the_ID() . '">' . __( 'read more', 'zoereel' ) . '</a>' );	
?>	

<div class="featureContainer" style="background-image: url(<?php echo esc_url($image[0]); ?>);">

				<article class="content-overlay">
					<div class="content-overlay-grad"></div>
					<div class="container-fluid rel">
						<div class="row rel">
							<div class="col-sm-5 col-xs-5 rel">
								<div class="synopis-outer">
									<div class="synopis-middle">
										<div class="synopis-inner">
											<h2><?php echo (isset($title) ? $title : __( 'No Title', 'zoereel' )); ?></h2>
											<div class="synopis content">
												<?php echo $content; ?>
												<ul class="hidden-xs">
<?php // do_action('synopis_multi_meta'); ?>
												</ul>
												
											</div>
										</div>
										<?php get_template_part('partials/video', 'metrics'); ?>
									</div>
								</div>
							</div>
							<div class="col-sm-7 col-xs-7 rel">
								<a class="play-icon-wrap" href="<?php the_permalink(); ?>">
									<div class="play-icon-wrap-rel">
										<div class="play-icon-wrap-rel-ring"></div>
										<span class="play-icon-wrap-rel-play">
											<i class="fa fa-play fa-3x" aria-hidden="true"></i>
							        	</span>
						        	</div>
					        	</a>
							</div>
						</div>
					</div>
				</article><!--/.content-overlay-->

			</div>

<?php endif; ?>