<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header>
  <div class="entry-summary">
  	<h3>Update your Job Posting</h3>
  	<?php acf_form(); ?>
  </div>
</article>
