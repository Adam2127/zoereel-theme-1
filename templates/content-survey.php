<?php
	$filmmaker_id = get_the_author_meta('ID');
	um_fetch_user($filmmaker_id);
	$profileurl = um_user_profile_url();
?>
<br>
<br>
<article id="surveyContent">
	<?php echo '<div id="postViews"><span>Survey Views</span> ' . do_shortcode('[post-views]') . '</div>'; ?>
	<?php do_action( 'notify_survey_view', $post->post_author ); ?>
	<header>
		<h2><?php // echo esc_html( get_the_title($vidID) ); ?>By: <a href="<?php echo $profileurl;?>"><?php the_author(); ?></a></h2>
	</header><br>
	<br>
	<div class="entry-summary">
		<div class="row">
			<div class="col-sm-10">
				<dl>
					<dt>Guess what is going to happen in the story?</dt>
					<dd><?php the_field('guess_what'); ?></dd>
					<dt>How would you change the storyline?</dt>
					<dd><?php the_field('how_would_you_change'); ?></dd>
					<dt>What was your favorite scene and why?</dt>
					<dd><?php the_field('favorite_scene'); ?></dd>
					<dt>How would you rate this content?</dt>
					<dd><?php the_field('rate_content'); ?></dd>
					<dt>How often do you watch movies?</dt>
					<dd><?php the_field('how_often_watch'); ?></dd>
					<dt>How did you like the characters, and who was your favorite?</dt>
					<dd><?php the_field('favorite_character'); ?></dd>
					<dt>Do you feel remakes of old films are good idea?</dt>
					<dd><?php the_field('remakes_good'); ?></dd>
					<dt>What is your favorite movie genre?</dt>
					<dd><?php the_field('favorite_genre'); ?></dd>
					<dt>Do you enjoy watching films more in the movie theatre or online?</dt>
					<dd><?php the_field('theatre_or_online'); ?></dd>
					<dt>Tell the studios what kind of films they should make?</dt>
					<dd><?php the_field('films_studios_should_make'); ?></dd>
				</dl>
			</div>
		</div><?php // the_excerpt(); ?>
	</div>
</article>

<?php

	// HACK: need to update $vidID with relationship fix: https://support.advancedcustomfields.com/forums/topic/relationship-field-not-saving/
	$surveyID = get_field('video_id');
	$vidID = get_post_meta($surveyID, 'video_id', true);
	$surveys = get_posts(array(
                'post_type' => array( 'survey' ), // uncomment it if you only want to get the result from movies post type
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'video_id',
                        'value' => '"' . $vidID . '"',
                        'compare' => 'LIKE'
                    )
                )
            ));
     $surveyNum = count($surveys);   
 	
 	// https://codex.wordpress.org/Class_Reference/wpdb#Protect_Queries_Against_SQL_Injection_Attacks
 	$wpdb->query( 
 	     $wpdb->prepare(
 	 		"
 	 			UPDATE $wpdb->posts 
 	 			SET surveys_count = %d
 	 			WHERE ID = %d
 	 		", 
 	          $surveyNum,
 	          $vidID
 	     )
 	); 	
?>