<?php acf_form_head(); ?>
<?php
	// NOTE: remove this conditional once unique link created
	$userVideo = false;	 // TODO: going to use unique link for editing
	if ( strpos($_SERVER['QUERY_STRING'], 'edit') === 0 ) {
		$userVideo = ( ($post->post_author == get_current_user_id() ) ? true : false );
	}

	if( $userVideo ) {
if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; }
if (is_super_admin()) { echo '<!--  seee acf_form_head -->'; }
	    acf_form_head();
if (is_super_admin()) { echo '<!--  end seee acf_form_head -->'; }
	}
?>

<!-- templates/content-single -->
<div id="back" onclick="goBack()">
    <span></span>
    <span></span>
</div>
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
<?php
	$file = get_field('video');
	if( $file ):
 		// $poster = get_the_post_thumbnail_url( '','poster-big' );
 		$poster = wp_get_attachment_image_src( get_field('cover_art'), 'poster-big' );
?>
	<div style="width: 1080px;" class="wp-video">
		<video id="video-<?php echo $post->ID;?>" poster="<?php echo $poster[0]; ?>" playsinline preload="metadata">
			<source type="video/mp4" src="<?php echo $file['url']; ?>" />
			<a href="<?php echo $file['url']; ?>"><?php echo $file['url']; ?></a>
		</video>
	</div>
<?php
	if( $userVideo ) {
?>
	<h1 class="entry-title"><?php the_title(); ?></h1>
    <div class="entry-content">
		<?php // the_content(); ?>
    </div>
<?php acf_form(); } ?>

	<?php endif; // END if Video $file ?>

<?php
	if( !$userVideo ) {
?>
<?php
 	// https://codex.wordpress.org/Class_Reference/wpdb
 	// $meta_key = 'wpfp_favorites';
 	// $alllikes = $wpdb->get_var( $wpdb->prepare(
 	// 	"
 	// 		SELECT sum(meta_value)
 	// 		FROM $wpdb->postmeta
 	// 		WHERE post_id = $post->ID
 	// 		AND meta_key = %s
 	// 	",
 	// 	$meta_key
 	// ) );

 	$reeltalkNum = $commentNum = 0;

 	$commentNum = get_comments(
 		array(
 		'status' => 'approve',
 		'post_id'=> $post->ID,
 		'type'=> 'comment',
 		'count' => true)
 	);
 	$reeltalkNum = get_comments(
 		array(
 		'status' => 'approve',
 		'post_id'=> $post->ID,
 		'type'=> 'reeltalk',
 		'count' => true)
 	);

 	// TODO: move this database update to trigger on realtalk submit
 	// https://codex.wordpress.org/Class_Reference/wpdb#Protect_Queries_Against_SQL_Injection_Attacks
 	$wpdb->query(
 	     $wpdb->prepare(
 	 		"
 	 			UPDATE $wpdb->posts
 	 			SET reeltalk_count = %d
 	 			WHERE ID = $post->ID
 	 		",
 	          $reeltalkNum
 	     )
 	);

?>
    <header id="offset">
      <h1 class="entry-title"><?php the_title(); ?></h1>
        <div class="entry-content">
			<p><?php the_field('video_description'); ?></p>
			<?php get_template_part('partials/entry', 'taxonomy'); ?>
    	</div>
      <?php get_template_part('partials/entry-meta'); ?>
    </header>
    <section class="option-bar">
		<?php echo do_shortcode('[post-views]'); ?>
    	<div <?php // echo wpfp_check_favorited($post->ID) ? 'class="liked"':''; ?> >
	    	<?php
		    	// echo do_shortcode( '[likebtn theme="custom" btn_size="28" f_size="16" icon_size="28" icon_l_c_v="#e0c264" icon_d_c_v="#e0c264" counter_l_c="#ffffff" bg_c="rgba(250,250,250,0)" brdr_c="rgba(198,198,198,0)" like_enabled="0" dislike_enabled="0" show_like_label="0" icon_dislike_show="0" white_label="1" popup_disabled="1" tooltip_enabled="0" rich_snippet="0" share_enabled="0"]' );
		    	echo do_shortcode('[likebtn theme="custom" btn_size="28" f_size="16" icon_size="28" icon_l_c_v="#e0c264" icon_d_c_v="#e0c264" counter_l_c="#ffffff" bg_c="rgba(250,250,250,0)" brdr_c="rgba(198,198,198,0)" like_enabled="0" dislike_enabled="0" show_like_label="0" icon_dislike_show="0" tooltip_enabled="0" white_label="1" rich_snippet="1" voter_by="user" user_logged_in="1" popup_disabled="1" popup_style="dark" share_enabled="0"]');
	    		// wpfp_link();
				// echo '<span class="like-count">'.$alllikes.'</span>';
	    	?>
	    </div>
	    <div><span id="makeComment" class="button-comment" href="#comment" onclick="document.cookie='open=comment'"><?php echo $commentNum; ?></span></div>
		<div id="sharediv">
			<?php echo do_shortcode('[easy-social-share sharebtn_style="icon" sharebtn_icon="share-outline" counters=0 style="button" point_type="simple" style="icon" buttons="share,facebook,twitter,google,pinterest,linkedin,blogger,reddit,tumblr,stumbleupon,mail"]'); ?> <span class="text">SHARE</span>
		</div>

		<div class="addtoplaylist-wrapper">
			<svg viewBox="2 2 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" style="pointer-events: none;display: block;width:3rem;height:3rem;">
				<path d="M14 10H2v2h12v-2zm0-4H2v2h12V6zm4 8v-4h-2v4h-4v2h4v4h2v-4h4v-2h-4zM2 16h8v-2H2v2z" class="addtoplaylist" style="color:#fff;fill:#fff;"></path>
			</svg>
			<?php echo do_shortcode('[cbxwpbookmarkbtn]'); ?>
		</div>
		<?php $reelbutton = '<div><span id="addReelTalk" class="button-reeltalk" onclick="document.cookie=\'open=reeltalk\'"><i class="um-faicon-comments-o"></i> ReelTalk ' . $reeltalkNum . '</span></div>';
			echo do_shortcode( '[um_show_content roles="artist,studio,admin"]' . $reelbutton . '[/um_show_content]' );
		?>
		<div>
		<span id="castCrew" class="button-castcrew" data-toggle="modal" data-target="#crewcastModal">Cast & Crew</span>
		</div>
		<?php
			echo do_shortcode( '[um_show_content roles="studio,admin"]<div><span id="inquire" class="" href="#">Inquire Project</span></div>[/um_show_content]' );
		?>
    </section>

<!-- Modal -->
<div id="crewcastModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">X</button>
        <h4 class="modal-title filmmaker"><?= get_the_author(); ?></h4> <h4 class="modal-title"><?= get_the_title(); ?></h4>
      </div>
      <div id="crewcastBody" class="modal-body">
	  	<section id="castCrewList" class="comments">
	  		<?php
	  		// check if the repeater field has rows of data
	  		if( have_rows('cast') ):
	  		echo '<h3>Cast</h3><table id="cast" class="castcrew">';
	  		 	// loop through the rows of data
	  		    while ( have_rows('cast') ) : the_row();
	  		        echo '<tr><td>';
	  		        	the_sub_field('cast_member', false);
	  		        echo '</td><td>';
	  		        	the_sub_field('character_name');
	  		        echo '</td></tr>';
	  		    endwhile;
	  		echo '</table>';
	  		else :
	  		    // no rows found
	  		    echo 'No Cast Listed';
	  		endif;

	  		// check if the repeater field has rows of data
	  		if( have_rows('crew') ):
	  		echo '<h3>Crew</h3><table id="crew" class="castcrew">';
	  		 	// loop through the rows of data
	  		    while ( have_rows('crew') ) : the_row();
	  		?>
	  		        <th><?php the_sub_field('department'); ?></th>

	  				<?php
	  					if( have_rows('position__name') ): while( have_rows('position__name') ): the_row();
	  		        		echo '<tr><td>';
	  		        		the_sub_field('crew_position', false);
	  						echo '</td><td>';
	  		        		the_sub_field('crew_name');
	  						echo '</td></tr>';
	  					endwhile; endif;
	  			endwhile;
	  		echo '</table>';
	  		else :
	  		    // no rows found
	  		    echo 'No Crew Listed <br>';
	  		endif;
	  		?>
	  	</section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <?php get_template_part('templates/form-inquire', get_post_type()); ?>
    <?php comments_template('/templates/comments.php'); ?>
    <?php comments_template('/templates/comments-rt.php'); ?>
    <?php get_template_part('templates/form-survey', get_post_type()); ?>

<script>
// document.cookie = "open=test";
function goBack() {
	    window.history.back();
	    console.log(document.referrer);
	}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
jQuery(window).load(function(){
	var offsetTop = parseInt( jQuery('#offset').css('paddingTop') );
//	console.log(offsetTop);

	jQuery('#makeComment').click(function(e){
		jQuery(document).cookie = "open=comment";
		jQuery('.comments').slideUp(300);
		jQuery('#comment').slideDown(300, function() {
			jQuery('html, body').animate({scrollTop: jQuery(this).offset().top - offsetTop }, 'fast');
		});
		jQuery('body').removeClass('reeltalking');
		jQuery('body').addClass('commenting');
	});
	jQuery('#addReelTalk').click(function(e){
		document.cookie = "open=reeltalk";
		jQuery('.comments').slideUp(300);
		jQuery('#reeltalk').slideDown(300, function() {
			jQuery('html, body').animate({scrollTop: jQuery(this).offset().top - offsetTop }, 'fast');
		});
		jQuery('body').removeClass('commenting');
		jQuery('body').addClass('reeltalking');
	});
	jQuery('#castCrew').click(function(e){
		document.cookie = "open=cast crew";
		jQuery('.comments').slideUp(300);
		jQuery('#castCrewList').slideDown(300);
	});
	jQuery('#inquire').click(function(e){
		jQuery('.comments').slideUp(300);
		jQuery('#inquiring').slideDown(300, function() {
			jQuery('html, body').animate({scrollTop: jQuery(this).offset().top - offsetTop }, 'fast');
		});
	});
	jQuery('#doSurvey').click(function(e){
		jQuery('.comments').slideUp(300);
		jQuery('#videoSurvey').slideDown(300, function() {
			jQuery('html, body').animate({scrollTop: jQuery(this).offset().top - offsetTop }, 'fast');
		});
	});

	jQuery("#reeltalk input[value='reeltalk']").prop( 'checked', true );

	if ( document.referrer.indexOf(location.protocol + "//" + location.host) === 0 ) {
		jQuery('#back').fadeIn();
		// console.log(document.referrer);
	} else {
		// jQuery('#goToGallery').fadeIn();
	}

  if(window.location.hash){
      var hash = window.location.hash.substr(1), open = hash.substr(0,hash.indexOf('\-'));
          open = (hash && !open) ? hash : open ;
console.log('hash is: ' + hash);
console.log('open is: ' + open);
	  	  open = open ? open : getCookie('open');
 	  if ( open ) {
 	    jQuery('#' + open ).css('display','block');
 	    jQuery('html,body').animate({ scrollTop: $("#"+open).offset().top }, 0);
 	    jQuery('#' + hash ).addClass('active-comment').animate({ scrollTop: $("#"+open).offset().top }, 0);
 	  }
  } else {
	 open = getCookie('open');
console.log('open is: ' + open);
 	 jQuery('.comments' ).css('display','none');
 	 jQuery('#' + open ).css('display','block');
  };
});
</script>

	    <?php if ( class_exists( 'Jetpack_RelatedPosts' ) ) { ?>
	    <section id="moreLike" class="clearfix">
		    <?php
				echo do_shortcode( '[jetpack-related-posts]' );
			?>
	    </section>
		<?php } ?>
	<?php } // END // if !$userVideo  ?>
    <footer>
      <?php // wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
<?php endwhile; ?>

