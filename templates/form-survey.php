<?php acf_form_head(); ?>
<section id="videoSurvey" class="comments">
	<div class="um"><h4 class="primary">Your Opinion <span>(<?php echo get_post_field( 'surveys_count', $post->ID ); ?> <span class="small">participants</span>)</span></h4></div>
	<?php
		acf_form('survey');
	?>
</section>

<script>
document.addEventListener("DOMContentLoaded", function(event) {
	z = document.querySelectorAll('input[name="acf[_post_title]"]');
	for (i = 0; i < z.length; i++) {
    	z[i].value = 'Submission on <?php the_title(); ?>  by <?php echo um_user('display_name') ?> at <?php echo current_time('Y-m-d H:i'); ?>';
	}
});		
</script>	