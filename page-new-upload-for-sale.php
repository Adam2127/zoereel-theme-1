<?php if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; } ?>
	<?php acf_form_head(); ?>

<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
	<article id="forsale">
		<?php acf_form('trailer_upload_for_sale'); ?>
	</article>	
		

<?php
	#TODO: move ACF select2_args filter javascript out of base.php
?>	
<script>
// jQuery( document ).ready(function() {
    jQuery('[data-name="video"] label').html('Trailer <span class="acf-required">*</span>');
    jQuery('[data-name="video_description"] label').html('Plot <span class="acf-required">*</span>');
    jQuery('[data-name="the_pitch"] label').html('The Pitch <span class="acf-required">*</span>');
// });
</script>	