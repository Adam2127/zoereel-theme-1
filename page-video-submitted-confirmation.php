<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php // get_template_part('templates/content', 'page'); ?>
  <?php 
	// $videoID = $_GET["v"];
	if (isset($_GET["v"])) {
		echo '<h4 class="submitted-message"><i>' . get_the_title($_GET["v"]) . '</i> ' . get_the_content() . '</h4>';
	} else {
		echo '<h4 class="submitted-message"><i>Your Video</i> ' . get_the_content() . '</h4>';
	}
  ?>
<?php endwhile; ?>
