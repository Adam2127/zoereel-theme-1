<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

  <div class="um-profile-body posts posts-default video-grid row">
	  <div class="row__inner">
<?php while (have_posts()) : the_post(); ?>
    <?php if( get_field('cover_art') && get_field('for_sale') != true ): ?>
	  		<?php // get_template_part('templates/content', 'search'); ?>
	  		<?php get_template_part('templates/video', 'grid-tile'); ?>
	  		<?php // $showmetrics = true; include( locate_template( 'templates/video-loop.php', false, false ) ); ?>
	<?php endif; ?>
<?php endwhile; ?>
	  </div>
  </div>
  
<?php // the_posts_navigation(); ?>
