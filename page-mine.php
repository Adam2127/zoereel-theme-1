<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php acf_form_head(); ?>
<?php
	$posterID = get_current_user_id();
	$userPost = ( ($post->post_author == $posterID ) ? true : false );
?>

<?php get_template_part('partials/nav', 'job-postings'); ?>

<section>

	<?php
	// $sticky = get_option( 'sticky_posts' );
	$args = array(
		'post_type'           => 'posting',
		'posts_per_page'      => 1000,
		'author'              => $posterID
	);

	$query = new WP_Query( $args );

		if ( $query->have_posts() ) :

		echo '<table>
			<tr>
				<th></th><th># of Submissions</th><th>Date Posted</th> <th>Title</th> <th>Project</th> <th>Casting Director</th> <th>Jobs</th> <th>Union</th>
			</tr>';

			while($query->have_posts()) : $query->the_post();

   $submissions = get_posts(array(
		'post_type' => array( 'submission' ), // uncomment it if you only want to get the result from movies post type
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'job_posting',
				'value' => '"' . $post->ID . '"',
				'compare' => 'LIKE'
			)
		)
	));

				echo '<tr>';
					echo '<td><a href="'.get_permalink().'?edit">edit</a></td>';
					echo '<td><a href="'.get_permalink().'#submissions">' . count($submissions)  . '</a></td>';
					echo '<td>'.get_the_date().'</td>';
					echo '<td><a href="'.get_permalink().'">'.get_the_title().'</a></td>';
					$term = get_term( get_field('production_type'), 'production_type' );
					echo '<td>'.$term->name.'</td>';
					echo '<td>'.get_field('casting_director').'</td>';
					echo '<td>'; ?>
					   <?php if( have_rows('production_jobs') ): ?>
					    <?php while( have_rows('production_jobs') ): the_row(); ?>
					        <span><?php echo the_sub_field('job_postion'); ?></span><br>
					    <?php endwhile; ?>
					   <?php endif; ?>
					<?php echo '</td>';
					echo '<td>'.get_field('union_staus').'</td>';
				echo '</tr>';

			endwhile;

			echo '</table>';

			wp_reset_postdata();

		else :  ?>

			<p style="margin: 3rem auto; text-align: center; font-size: 1.5rem;"><?php _e( 'You don\'t have any Job Postings', 'sage' ); ?></p>

		<?php endif; ?>

</section>