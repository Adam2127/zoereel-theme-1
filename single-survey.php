<?php // acf_form_head(); ?>

<?php
	$showSurvey = ( $post->post_author == get_current_user_id() ? true : false );
	$confirrmation = $_SERVER['QUERY_STRING'];
	$videoSurvey = (isset($_GET['submitted'])) ? $_GET['submitted'] : get_the_ID();
	// if (strpos($confirrmation, 'submitted') !== false || $showSurvey == true) {
	if (strpos($confirrmation, 'submitted') !== false ) {
?>

	<div id="confirmation-messaage">
		<h4>You just made an impact on the production process in Hollywood, decide what do you want to watch.</h4>
		<br>
		<br>
		<h4>Keep Changing!</h4>
		<h4>Thank you!</h4>
	</div>

<?php } else { ?>

<?php get_template_part('templates/content-survey', get_post_type()); ?>

<?php } ?>

<?php

$vidID = get_post_meta($videoSurvey, 'video_id', true);

	$surveys = get_posts(array(
                'post_type' => array( 'survey' ),
                'posts_per_page' => -1,
                 'meta_query' => array(
                     array(
                         'key' => 'video_id',
                         'value' =>  $vidID,
                         'compare' => 'LIKE'
                     )
                 )
            ));
     $surveyNum = count($surveys);
     echo '<br> - $surveyNum: ' . $surveyNum;

 	// https://codex.wordpress.org/Class_Reference/wpdb#Protect_Queries_Against_SQL_Injection_Attacks
 	$wpdb->query(
 	     $wpdb->prepare(
 	 		"
 	 			UPDATE $wpdb->posts
 	 			SET surveys_count = %d
 	 			WHERE ID = %d
 	 		",
 	          $surveyNum,
 	          $vidID
 	     )
 	);

?>
