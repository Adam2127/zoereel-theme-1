<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/favicon.ico"/>
  <title>Zoereel | Artist to Audience to Studio | Disallowed Country</title>
<meta name='robots' content='noindex,follow' />

<link rel="stylesheet" href="/app/themes/zoereel/assets/styles/static/country-block.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>(window.jQuery && jQuery.noConflict()) || document.write('<script src="/wp/wp-includes/js/jquery/jquery.js"><\/script>')</script>
<script src="/app/plugins/ip-geo-block/admin/js/authenticate.min.js"></script>

</head>

<!-- http://www.ipgeoblock.com/codex/customizing-the-response.html -->

    <body class="error400">
      
		<header class="banner" id="header">
			<div class="container">
				<a class="brand" href="https://zoereel.com/"><img alt="Zoereel" src="/app/themes/zoereel/dist/images/color-white-text.svg"><span>Beta</span></a>
				<h3 class="outlinetext">Where New Ideas in TV & Film Live</h3>
			</div>
		</header>
		<div class="wrap container" role="document">
			<div class="content">
				<main class="main">
					<div class="page-header">
						<h1>Sorry, Zoereel is not available in your region yet</h1>
					</div><!--
		<div class="alert alert-warning">
		  Sorry, but the page you were trying to view does not exist.</div>
		-->
				</main><!-- /.main -->
			</div><!-- /.content -->
		</div><!-- /.wrap -->
		<footer class="content-info">
			<div class="container"></div>
		</footer>
 
  </body>
</html>