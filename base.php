<?php
if ( !is_user_logged_in() && is_front_page() ) {
	$_SESSION['landing'] = null;
} elseif ( !is_user_logged_in() && !is_page( array( 1478, 114, 1511, 1518, 1532, 2164, 124) ) ) { // 1478 - signup, 114 - login, 1511 - register studio, 1518 - register audience, 1532 - register filmmaker, 2164 - register all, 124 -  password reset
    // header( 'Location: ' . get_permalink(2164) );
	echo '<script>window.location.href = "' . get_permalink(2164) . '";</script>';
} elseif (is_page( 2164 )) {
	if (isset($_SESSION['landing']) && $_SESSION['landing'] == 'landing-seen') {
		$landing = 'seen';
	} else {
		$_SESSION['landing'] = 'landing-seen';
		$landing = 'unseen';
	}
}
?>
<?php

use Roots\Sage\Setup; // in lib/setup.php
use Roots\Sage\Wrapper;

$isMobile = wp_is_mobile() ? '-mobile' : '';
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <?php if (is_page( 147800000 )) { ?>
  <body <?php body_class('browser'.$isMobile. ' '.$landing); ?>>
  <?php } else { ?>
  <body <?php body_class('browser'.$isMobile); ?>>
  <?php } ?>
<?php // acf_form_head(); ?>
    <?php
      do_action('get_header');
        get_template_part('templates'.$isMobile.'/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
<script type="text/javascript">
//TODO: move out of base
// (function($) {
//
// 	acf.add_filter('select2_args', function( args, $select, settings ){
// 		// limit to 3
// 		args.maximumSelectionSize = 3;
// 		args.maximumSelectionLength = 3;
// 		// return
// 		return args;
// 	});
// })(jQuery);
if( typeof acf !== 'undefined' ) {
	acf.conditional_logic.add( 'field_59bb052f17e62', [[{"field":"field_599f8abc1258f","operator":"==","value":"Content"}]] );
	acf.conditional_logic.add( 'field_5a08e2c293a96', [[{"field":"field_599f8abc1258f","operator":"==","value":"Reel"}]] );
	acf.conditional_logic.add( 'field_59bb10b7be067', [[{"field":"field_599f8abc1258f","operator":"==","value":"Content"}]] );
	acf.conditional_logic.add( 'field_596141d815bb3', [[{"field":"field_599f8abc1258f","operator":"==","value":"Content"}]] );
	acf.conditional_logic.add( 'field_59613db74038f', [[{"field":"field_599f8abc1258f","operator":"==","value":"Content"}]] );
	acf.conditional_logic.add( 'field_597baf549e00a', [[{"field":"field_5977c9bf099d3","operator":"==","value":"1"}]] );
	acf.conditional_logic.add( 'field_5a0a480d80ba5', [[{"field":"field_599f8abc1258f","operator":"==","value":"Content"}]] );
	  acf.conditional_logic.add( 'field_596d6425cb839', [[{"field":"field_596a5155a1ef1","operator":"==","value":"Other"}]] );
	  acf.conditional_logic.add( 'field_596a53d3a1ef8', [[{"field":"field_596a5380a1ef7","operator":"!=","value":"No"}]] );
}
</script>

<?php if($isMobile == '-mobile') { ?>

<script type="text/javascript">
    amzn_assoc_ad_type = "link_enhancement_widget";
    amzn_assoc_tracking_id = "zoereel-20";
    amzn_assoc_linkid = "9e5ef2ce94842c4ecf71b1e9274cf1a1";
    amzn_assoc_placement = "";
    amzn_assoc_marketplace = "amazon";
    amzn_assoc_region = "US";
</script>
<script src="//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1&MarketPlace=US"></script>

<?php } ?>

  </body>
</html>