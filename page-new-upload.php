<?php if (is_super_admin()) { echo '<!--  ' . basename(__FILE__) . ' -->'; } ?>
	<?php acf_form_head(); ?>

<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

	<?php acf_form('video_upload'); ?>

<?php
	#TODO: move ACF select2_args filter javascript out of base.php
?>	