<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php acf_form_head(); ?>
<?php
	// $posterID = get_current_user_id();
	$userPost = ( ($post->post_author == get_current_user_id() ) ? true : false );
?>
<?php get_template_part('partials/nav', 'job-postings'); ?>

<section>
	
	<table>
		<tr>
			<th>Date Posted</th> <th>Title</th> <th>Project</th> <th>Casting Director</th> <th>Jobs</th> <th>Union</th>
		</tr>	
			<?php
	$args = array(
		'post_type' => 'posting',
		'posts_per_page'      => 1000,
	);
	
	// https://codex.wordpress.org/Function_Reference/the_date
	
	$query = new WP_Query( $args );
			while($query->have_posts()) : $query->the_post();
			
				echo '<tr>';
					// echo '<td>'.($posterID == get_the_author_meta('ID') ? '<a href="'.get_permalink().'?edit">edit</a>' : '' ).'</td>';	
					echo '<td>'.get_the_date().'</td>';
					echo '<td><a href="'.get_permalink().'">'.get_the_title().'</a></td>';
					$term = get_term( get_field('production_type'), 'production_type' );
					echo '<td>'.$term->name.'</td>';
					echo '<td>'.get_field('casting_director').'</td>';
					echo '<td>'; ?>
					   <?php if( have_rows('production_jobs') ): ?>
					    <?php while( have_rows('production_jobs') ): the_row(); ?>		 
					        <span><?php echo the_sub_field('job_postion'); ?></span><br>	        
					    <?php endwhile; ?>	 
					   <?php endif; ?> 
					<?php echo '</td>';
					echo '<td>'.get_field('union_staus').'</td>';
				echo '</tr>';   
			
			endwhile;
			
			wp_reset_postdata();
			?>
	</table>

</section>