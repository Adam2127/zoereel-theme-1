<?php
	$filmmaker_id = get_the_author_meta('ID');
	um_fetch_user($filmmaker_id);
?>

<!-- templates/entry-meta -->
<!-- <time class="updated" datetime="<// ?= get_post_time('c', true); ?>"><// ?= get_the_date(); ?></time> -->
<div class="credit um-followers-bar">
	<p class="byline author vcard"><?= __('By:', 'zoereel'); ?> <a href="<?= um_user_profile_url(); ?>" rel="author" class="fn"><?= get_the_author(); ?></a></p>
	<section class="um"><?php echo do_shortcode('[ultimatemember_followers_bar user_id='.get_the_author_meta('ID').']'); ?></section>
	<?php // echo (isset($showcredits)) ? '<a style="position:relative;bottom:1px;" class="btn btn-outline-primary" href="#">' . __('Cast & Crew', 'zoereel') . '</a>' : ''; ?>
	<button id="doSurvey" class="btn btn-outline-primary"><i class="um-icon-mic-a"></i> Your Opinion <span>(<?php echo get_post_field( 'surveys_count', $post->ID ); ?>)</span></button>
</div>
