<!-- partials/comment-poster -->
<div class="zr-item-poster">
   <a href="<?php the_permalink($vidID); ?>"><?php echo wp_get_attachment_image( get_field('cover_art', $vidID), 'medium' ); ?></a>
   <div class="content-overlay">
   	<a class="play-icon-wrap" href="<?php the_permalink($vidID); ?>">
    	 	<div class="play-icon-wrap-rel-ring"></div>
   	 	<span class="play-icon-wrap-rel-play"><i class="fa fa-play fa-2x" aria-hidden="true"></i></span>
    	</a>
   </div>
</div>