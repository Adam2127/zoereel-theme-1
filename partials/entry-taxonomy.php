<div class="video-meta">
<?php
	$categories = get_the_category();
	// $genres = __( 'Genres', 'streamium' ) . ": ";
	$categoryoutput = '';
	if ($categories) {
		$numItems = count($categories);
		$i = 0;
	  	foreach($categories as $cat) {

	  		// $categoryoutput .= '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . ucwords($cat->name) . '</a>';
	  		$categoryoutput .= '<span>' . ucwords($cat->name) . '</span>';
	  		if(++$i !== $numItems) {
	    		$categoryoutput .= ', ';
	  		}

	  	}
	  	echo $categoryoutput . ': ';
	}
	$genres = get_the_terms(get_the_ID(),'genre');
	// $genreoutput = __( 'Cast', 'streamium' ) . ": ";
	$genreoutput = '';
	if ($genres) {
		$numItems = count($genres);
		$i = 0;
	  	foreach($genres as $genre) {

		  	$genreoutput .= '<a href="/?s=' . esc_html( $genre->name ) . '">' . ucwords($genre->name) . '</a>';
		  	if(++$i !== $numItems) {
	    		$genreoutput .= ', ';
	  		}

	    }
	    echo $genreoutput;
	}	
?>					
</div>