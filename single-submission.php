<?php // acf_form_head(); ?>
<?php get_template_part('partials/nav', 'job-postings'); ?>
<?php // $posterID = get_current_user_id(); ?>
<?php // if ( strpos($_SERVER['QUERY_STRING'], 'edit') === 0 && $posterID == get_the_author_meta('ID') ) { 
	// get_template_part('templates/content-posting-edit', get_post_type());
	// } else {
	// get_template_part('templates/content-posting', get_post_type());
	// acf_form('submission');
?>
	<script>
		// document.getElementById("acf-_post_title").value = 'Submission: <?php the_title(); ?>';	
	</script>	
<?php
	// }
?>	
<?php 
	$jobPosting = get_field('job_posting');
	// var_dump( $jobPosting);
?>
<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header>
  <div class="entry-summary">
	<div class="flex">  
      	<div class="col-sm-6">
    	  <dl>
    		  <dt>Applicant</dt>
    		  <dd><?php echo get_the_author_link(); ?></dd> 
    	  </dl>
      	</div>
	</div>
	<div class="">  
      	<div class="col-sm-6">
    	  <dl>
    		  <dt>Job Applying For</dt>
    		  <dd><?php the_field('job_applying_for'); ?></dd> 
    	  </dl>
      	</div>
      	<div class="col-sm-6">  	  
    	  <dl>		  
    		  <dt>Link to video reel</dt>
    		  <dd><a href="<?php the_field('link_to_reel'); ?>" target="_blank"><?php the_field('link_to_reel'); ?></a></dd>		   
    	  </dl>
      	</div>  
	</div>
  </div>
</article>

<?php 
	$userSumbmission = ( ($post->post_author == get_current_user_id() ) ? true : false ); 
	$authorID = get_the_author_meta('ID');
?>

<section id="objectID" class="<?php echo ($userSumbmission) ? 'submittor': 'recruiter'; ?>" data-artist="<?php echo $authorID ?>" data-zoereel_vid="<?php echo $post->ID;?>" data-object_id="<?php echo $post->ID;?>">
    <div id="dealbutton">
    <?php 
	    $shortcodepanel = sprintf(
		    '[ultimatemember_messages user_id="%1$s", conversation_id="%2$s"]',get_current_user_id(), $post->ID
		);
// 		echo $shortcodepanel;
	    echo do_shortcode($shortcodepanel);
	?>
    </div>
</section>